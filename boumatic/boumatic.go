package boumatic

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
)

const BOUMATIC = "boumatic"

//                    Linux          Windows 
//                    =============  =====================
// os.UserHomeDir()   $HOME          $HOME
// os.UserConfigDir() $HOME/.config  $HOME\AppData\Roaming
// os.UserCacheDir()  $HOME/.cache   $HOME\AppData\Local

// Returns folder {$HOME}/.config/boumatic for linux.
// Returns {$HOME}\AppData\Roaming\boumatic for windows.
// Works for linux and windows
func UserConfig() (string, error) {
 	config, err := os.UserConfigDir()
 	if err != nil {
 		return "", err
 	}
	base := filepath.Join(config, BOUMATIC)
	if _, err := os.Stat(base); errors.Is(err, os.ErrNotExist) {
		err := os.Mkdir(base, os.ModePerm)
		if err != nil {
			return "", err
		}
	}
	return base, nil
}

// DataFolder returns folder where to store databases
//	- Linux: returns /var/lib/boumatic/{folder}
//	- Windows: returns $HOME\AppData\Local\boumatic\{folder}
func DataFolder(folder string) (string, error) {
	
	switch runtime.GOOS {
	
	case "linux":
		local := "/var/lib"
		return dataFolder(local, folder)

	case "windows":
	 	local, err := os.UserCacheDir() //$HOME\AppData\Local
	 	if err != nil {
	 		return "", err
	 	}
	 	return dataFolder(local, folder)

	default:
		return "", fmt.Errorf("Invalid operating system: %s", runtime.GOOS)

	}
}

func dataFolder(local, folder string) (string, error) {

	boumatic := filepath.Join(local, BOUMATIC)
	if _, err := os.Stat(boumatic); errors.Is(err, os.ErrNotExist) {
		// try to create boumatic folder
		err := os.Mkdir(boumatic, os.ModePerm)
		if err != nil {
			return "", err
		}
	}

	base := filepath.Join(boumatic, folder)
	if _, err := os.Stat(base); errors.Is(err, os.ErrNotExist) {
		// try to create folder 
		err := os.Mkdir(base, os.ModePerm)
		if err != nil {
			return "", err
		}
	}
	return base, nil
}

