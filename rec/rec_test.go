package rec

import (
	"context"
	"testing"
	"time"
)

func TestWriter(t *testing.T) {

	folder := "test-cans-milk-recs"

	ctx, cancel := context.WithTimeout(context.Background(), 6 * time.Second)
	defer cancel()
	t.Log("wait 6 seconds...")
	cfg := CfgDefault()
	cfg.Log = 1
	_, err := NewWriter(ctx, cfg, folder)
	if err != nil {
		t.Skip(err)
	}

	ctx, end := context.WithCancel(ctx)
	defer end()
	t.Log("Default cfg should flushes in 5 sec...")
	for {
		select {
		case <-ctx.Done():
			return
		}
	}
}