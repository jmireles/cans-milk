package rec

import (
	"fmt"
	"time"

	"github.com/rivo/tview"
	"gitlab.com/jmireles/cans-base"

	"gitlab.com/jmireles/cans-milk/term"
)

const (
	cfgSaveDefault = 5 * time.Second
)

type Cfg struct {
	Save base.Time `json:"save"`
	Log  int       `json:"log"`
}

func CfgDefault() *Cfg {
	return &Cfg{
		Save: base.Time{ cfgSaveDefault },
	}
}

func (c *Cfg) Info() string {
	return fmt.Sprintf(
		`{"save":%q,"log":%d}`,
		c.Save, c.Log,
	)
}

func (c *Cfg) Form(form *tview.Form) {

	if c == nil {
		return
	}

	term.NewDropDown("Save",

		term.DropItemDuration(cfgSaveDefault),

	).AddForm(form, term.DropItemDuration(c.Save.Duration), func(item term.DropItem) {
		c.Save.Duration = time.Duration(item.(term.DropItemDuration))
	})
	
	term.NewDropDown("Log",
		term.DropItemInt(0),
		term.DropItemInt(1),
		term.DropItemInt(2),
	).AddForm(form, term.DropItemInt(c.Log), func(item term.DropItem) {
		c.Log = int(item.(term.DropItemInt))
	})
}

func (c *Cfg) Normalize() {
	if min := cfgSaveDefault; c.Save.LowerThan(min) {
		c.Save.Duration = min
	}
}

func CfgHelp(h *term.Help) {

	h.Print2W("Recs writer")

	h.Print4BG("Enable",
		`Click to enable CAN events recording.`)

	h.Print4YG("Save",
		`Select the frequency to flush the records to disk.`)

	h.Print4YG("Logs",
		`Select the level for logging:
        == 0 - None.
        >= 1 - Basic logging.
        >= 2 - Complete logging.`)

	h.Print4BG("Disable",
		`Click to disable CAN events recording.`)
}





