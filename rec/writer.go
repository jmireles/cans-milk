package rec

import (
	"context"
	"fmt"
	"os"
	"time"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/line"
	"gitlab.com/jmireles/cans-base/milk/recs"

	"gitlab.com/jmireles/cans-milk/boumatic"
)


// Writer is used to write milk line records
type Writer struct {
	cfg  *Cfg
	recs *recs.Recs
	log  base.Log
	hbs  int
}


// NewWriter is used to write records in folder
//	- Linux /var/lib/boumatic/{folder}
//	- Windows $HOME\AppData\Local\boumatic\{folder}
func NewWriter(ctx context.Context, cfg *Cfg, folder string) (*Writer, error) {
	path, err := boumatic.DataFolder(folder)
	if err != nil {
		return nil, err
	}
	return newWriter(ctx, cfg, path, 0644), nil
}

// NewWriterTmp is used to write records in temporary folders
// with permission 0777
func NewWriterTmp(ctx context.Context, cfg *Cfg, tmp string) *Writer {

	return newWriter(ctx, cfg, tmp, 0777)
}

func newWriter(ctx context.Context, cfg *Cfg, folder string, mode os.FileMode) *Writer {
	w := &Writer{
		cfg:  cfg,
		recs: recs.NewRecs("", folder, mode, recs.YMD_NLRI),
	}
	if cfg.Log > 0 {
		w.log = func(f string, args ...any) {
			fmt.Print("Recs writer: ")
			fmt.Printf(f, args...)
			fmt.Println()
		}
	}

	go w.flusher(ctx)

	return w
}

func (r *Writer) flusher(ctx context.Context) {

	// Record PC started
	r.appendPC(true)

	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	ticker := time.NewTicker(r.cfg.Save.Duration)
	defer ticker.Stop()
	for {
		select {
		
		case <-ticker.C:
			// save accumulated records
			r.Flush()

		case <-ctx.Done():
			// Record PC stopped
			r.appendPC(false)
			r.Flush()
			time.Sleep(100 * time.Millisecond)
			return
		}
	}
}



func (r *Writer) IncrHB() {
	r.hbs++
}

func (r *Writer) appendPC(start bool) *recs.Record {
	now, _ := base.UTCNow()
	var record *recs.Record
	if start {
		record = recs.NewRecordBox8(0, 0, []byte{1}, now)
	} else {
		record = recs.NewRecordBox8(0, 0, []byte{2}, now)
	}
	if r.cfg.Log > 0 {
		r.logAppend("PC", record, nil)
	}
	r.recs.Append(record)
	return record
}

func (r *Writer) AppendMsg(msg *line.Msg) {
	record, err := recs.NewRecordMsg(msg)
	if r.cfg.Log > 1 {
		s := fmt.Sprintf("msg %02x hbs=%d", msg.Data, r.hbs)
		r.logAppend(s, record, err)
	}
	if err == nil {
		r.recs.Append(record)
	}
}

func (r *Writer) AppendResp(resp from.Resp) {
	record, err := recs.NewRecordResp(resp)
	if r.cfg.Log > 1 {
		s := fmt.Sprintf("resp %02x hbs=%d", resp.Bytes(), r.hbs)
		r.logAppend(s, record, err)
	}
	if err == nil {
		r.recs.Append(record)
	}
}

// Save saves zero or more records appended to buffer
func (r *Writer) Flush() {
	saved, err := r.recs.Save()
	if r.cfg.Log > 0 {
		if err != nil {
			r.log("%sSave error: %v%s", base.Yellow, err, base.Reset)
		} else if len(saved) > 0 {
			r.log("%sSave: %v%s", base.Blue, saved, base.Reset)
		}
	}
}

func (r *Writer) logAppend(msg string, rec *recs.Record, err error) {
	if err != nil {
		r.log("%sAppend skip %v msg=%s%s", base.Yellow, err, msg, base.Reset)
	} else {
		r.log("%sAppend ok %s%s %s%s", base.Green, rec.NLRI(), rec.Date(), rec.RowDebug(), base.Reset)	
	}
}

func (r *Writer) End(zip bool) error {
	record := r.appendPC(false) // save app stopped
	r.Flush() // flush everything pending

	var logFS func(string, int64)
	if r.cfg.Log > 0 {
		logFS = func(name string, size int64) {
			r.log("%sFile %v %v%s", base.Blue, name, size, base.Reset)
		}
	}
	// print all folder/files
	err := r.recs.Files(logFS)
	if err != nil {
		return err
	}
	if zip {
		// zip last-date folder
		err = r.recs.Zip(record.Date(), true, func(f string, args ...any) {
			r.log(f, args...)
		})
		if err != nil {
			return err
		}
		// print folder zipped
		return r.recs.Files(logFS)
	} else {
		return nil
	}
}

