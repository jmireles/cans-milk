package rec

import (
	"context"
	"fmt"
	"io/fs"
	"io/ioutil"
	"path/filepath"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
	"gitlab.com/jmireles/cans-base/milk/recs"
	
	"gitlab.com/jmireles/cans-milk/boumatic"
	"gitlab.com/jmireles/cans-milk/term"
)

type History struct {

	*term.Term

	name    string
	version string
	path    string
}

func NewHistory(ctx context.Context, name, version string) error {

	path, err := boumatic.DataFolder(name)
	if err != nil {
		return err
	}
	t := term.NewTerm()
	g := &History{
		name:    name,
		version: version,
		path:    path,
		Term:    t,
	}

	return t.RunGrid(tview.NewGrid().
		SetRows(1, 0).
		SetColumns(16, 0).
		AddItem(g.Header(),   0, 0, 1,  2, 0, 0, false).
		AddItem(g.controls(), 1, 0, 1,  1, 0, 0, false).
		AddItem(t.Center(),   1, 1, 1,  1, 0, 0, false))
}

func (g *History) Header() *tview.TextView {
	header := tview.NewTextView().
		SetTextAlign(tview.AlignCenter).
		SetDynamicColors(true)
	fmt.Fprintf(header, "[white]Records reader [green]%s[white] version [yellow]%s",
		g.name, g.version)
	return header
}

func (g *History) controls() *tview.Flex {
	t := tview.NewFlex().
		SetDirection(tview.FlexRow).
		AddItem(g.fileSelectorTree(nil), 0, 1, true)
	t.SetBorder(true)
	return t
}

func (g *History) fileSelectorTree(filters []string) *tview.TreeView {
	accept := func(file fs.FileInfo) bool {
		return true
	}
	// A helper function which adds the files and directories of the given path
	// to the given target node.
	add := func(target *tview.TreeNode, path string) {
		files, err := ioutil.ReadDir(path)
		if err != nil {
			panic(err)
		}
		for _, file := range files {
			if accept(file) {
				node := tview.NewTreeNode(file.Name()).
					SetReference(&FileRef{
						path: filepath.Join(path, file.Name()),
						file: file,
					}).
					SetSelectable(true)
				if file.IsDir() {
					node.SetColor(tcell.ColorGreen)
				}
				target.AddChild(node)
			}
		}
	}
	rootDir := g.path
	root := tview.NewTreeNode("recs").SetColor(tcell.ColorRed)
	add(root, rootDir)
	return tview.NewTreeView().
		SetRoot(root).
		SetCurrentNode(root).
		SetDoneFunc(func(k tcell.Key) {
			// Esc,Tab,Back abort
			//g.file("")
		}).
		SetSelectedFunc(func(node *tview.TreeNode) {
			reference := node.GetReference()
			if reference == nil {
				g.file("")
				return // Selecting the root node does nothing.
			}
			children := node.GetChildren()
			if len(children) == 0 {
				// Load and show files in this directory.
				ref := reference.(*FileRef)
				if ref.file.IsDir() {
					add(node, ref.path)
				} else {
					// Filename was clicked, selection done
					g.file(ref.path)
				}
			} else {
				// Collapse if visible, expand if collapsed.
				node.SetExpanded(!node.IsExpanded())
			}
		})
}

func (g *History) file(path string) {

	tv := tview.NewTextView().
		SetDynamicColors(true)
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Fprintf(tv, "[red]%v", err)
	} else if size := len(bytes); size % 12 != 0 {
		fmt.Fprintf(tv, "[red]Invalid size:%d", size)
	} else {
		j := 0
		for i := 0; i < size ; i += 12 {
			row := bytes[i : i+12]

			j++
			fmt.Fprintf(tv, "% 6d ", j)

			// timestamp
			h, m, s, c := recs.RowTime(row)
			fmt.Fprintf(tv, "%02d:%02d:%02d.%02d", h, m, s, c)

			fmt.Fprintf(tv, " %02x", row[3])

			fmt.Fprintf(tv, " %02x\n", row[4:])

		}
	}
	g.Term.Center().
		Clear().
		AddItem(tv, 0, 1, false).
		SetBorder(true).
		SetTitle(path)
}

type FileRef struct {
	path string
	file fs.FileInfo
}
