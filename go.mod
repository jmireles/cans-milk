module gitlab.com/jmireles/cans-milk

go 1.19

require (
	github.com/gdamore/tcell/v2 v2.6.0
	github.com/rivo/tview v0.0.0-20230530133550-8bd761dda819
	golang.org/x/net v0.7.0
	golang.org/x/text v0.7.0
)

replace gitlab.com/jmireles/cans-base => ../cans-base

require (
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/rivo/uniseg v0.4.3 // indirect
	gitlab.com/jmireles/cans-base v0.0.0
	golang.org/x/sys v0.5.0 // indirect
	golang.org/x/term v0.5.0 // indirect
)
