
# cans-milk
```
     ,-----------,
     | cans-base |
     '-----o-----'
           |     
     ,-----^-----,
     | cans-milk |
     '-----------'
```

## app

`/cans-milk$ go run ./apps/cmd/.`

```
Usage of cans-milk:
  -h              Prints this help and exit.
  gui             Gui for cans-milk cfgs.
  history         History for app.
  history -h      History params help.
  reverse         Reverse proxy.
  reverse -h      Reverse proxy params help.
  velos-client    Velos client to test velos server.
  velos-client -h Velos client params help.
  velos-server    Velos server to test velos clients.
  velos-server -h Velos server params help.
```