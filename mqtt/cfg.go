package mqtt

import (
	"fmt"

	"github.com/rivo/tview"

	"gitlab.com/jmireles/cans-milk/term"
)

const (
	cfgDefaultPort   = 1883
	cfgEncryptedPort = 8883
)

type Cfg struct {
	Port int    `json:"port"`
	User string `json:"user"`
	Pass string `json:"pass"`
	PM   bool   `json:"pm"`
	T4   bool   `json:"t4"`
	I4   bool   `json:"i4"`
	RSI  bool   `json:"rs"`
	Log  int    `json:"log"`
}

func CfgDefault() *Cfg {
	return &Cfg{
		Port: cfgDefaultPort,
	}
}

func (c *Cfg) Form(form *tview.Form) {

	if c == nil {
		return 
	}
	term.NewDropDown("Port",

		term.DropItemInt(cfgDefaultPort),
		term.DropItemInt(cfgEncryptedPort),

	).AddForm(form, term.DropItemInt(c.Port), func(item term.DropItem) {
		c.Port = int(item.(term.DropItemInt))
	}).
	AddInputField("Username", c.User, 30, nil, func(user string) {
		c.User = user
	}).
	AddPasswordField("Password", c.Pass, 30, '*', func(pass string) {
		c.Pass = pass
	})
	
	form.AddFormItem(tview.NewCheckbox().
			SetLabel(" PM ").
			SetCheckedString(" ✔ ").
			SetChecked(c.PM).
			SetChangedFunc(func(checked bool) {
				c.PM = checked
			})).
		AddFormItem(tview.NewCheckbox().
			SetLabel(" T4 ").
			SetCheckedString(" ✔ ").
			SetChecked(c.T4).
			SetChangedFunc(func(checked bool) {
				c.T4 = checked
			})).
		AddFormItem(tview.NewCheckbox().
			SetLabel(" I4 ").
			SetCheckedString(" ✔ ").
			SetChecked(c.I4).
			SetChangedFunc(func(checked bool) {
				c.I4 = checked
			})).
		AddFormItem(tview.NewCheckbox().
			SetLabel(" RSI ").
			SetCheckedString(" ✔ ").
			SetChecked(c.RSI).
			SetChangedFunc(func(checked bool) {
				c.RSI = checked
			}))
	
	term.NewDropDown("Log",
		term.DropItemInt(0),
		term.DropItemInt(1),
		term.DropItemInt(2),
	).AddForm(form, term.DropItemInt(c.Log), func(item term.DropItem) {
		c.Log = int(item.(term.DropItemInt))
	})
}

func (c *Cfg) Info() string {
	return fmt.Sprintf(
		`{"port":%d,"user":%q,"lines":[%t,%t,%t,%t],"log":%d}`,
		c.Port, c.User, c.PM, c.T4, c.I4, c.RSI, c.Log,
	)
}

func (c *Cfg) Normalize() {
	switch c.Port {
	case cfgDefaultPort,
		cfgEncryptedPort:
		
	default:
		c.Port = cfgDefaultPort
	}
}

func CfgHelp(h *term.Help) {

	h.Print2W("MQTT broker")

	h.Print4BG("Enable", 
		`Enable the MQTT server to dispatch CAN events.`)
	
	h.Print4YG("Port",
		`Select the MQTT broker port number.`)

	h.Print4YG("Username",
		`Type the username to match authenticated clients.`)

	h.Print4YG("Password",
		`Type the password to match authenticated clients.`)

	h.Print4YG("PM",
		`Publish Pulsator Monitor events (future).`)

	h.Print4YG("T4",
		`Publich Takeoff-4 events such starts,stops,manuals.`)

	h.Print4YG("I4",
		`Publish Input-4 events (future).`)

	h.Print4YG("RSI",
		`Publish Rotary Stall Identifier events (future).`)

	h.Print4YG("Log",
		`Select the level for logging:
        0 - None.
        1 - Console.`)

	h.Print4BG("Disable",
		`Disable the MQTT broker.`)
}

