package apps

import (
	"fmt"
	"os"
	"testing"

	"gitlab.com/jmireles/cans-base"

)

func TestGarbler(t *testing.T) {

	tt := base.Test{ T:t }

	garbler := NewAESGarbler([]byte("123456789012345678901234"))

	json := `{"bool":true,"int":42,"string":"hello"}`

	saved, err := garbler.Save([]byte(json))
	tt.Equals("tnLRc8/RB6lkQjkprzNaVfEhoi9rSCL94ESJp/ejmLYUdMi2j66Z", string(saved))
	tt.Ok(err)

	read, err := garbler.Read(saved)
	tt.Equals(json, string(read))
	tt.Ok(err)
}

func TestCfgs(t *testing.T) {

	type Json struct {
		Bool   bool   `json:"bool"`
		Int    int    `json:"int"`
		String string `json:"string"`
	}
	save := &Json{
		Bool:   true,
		Int:    43,
		String: "hello",
	}

	tt := base.Test{ T:t }

 	home, _ := os.UserHomeDir()
	cfg1, err := newCfgs("test-cans-milk-1", "1.2.3", nil)
	tt.Ok(err)

	tt.Equals(fmt.Sprintf("%s/.config/boumatic/test-cans-milk-1.cfg", home), cfg1.path())
	tt.Equals("1.2.3", cfg1.version)

	// Save 1 without Garbler
	tt.Ok(cfg1.Save(save))

	var read1 Json
	tt.Ok(cfg1.Read(&read1))

	tt.Equals(save, &read1)

	// Save 1 with garbler
	cfg2, err := NewCfgs("test-cans-milk-2", "1.2.3", "_-^-_-^-_-^-_-^-")
	tt.Ok(err)

	tt.Ok(cfg2.Save(save))

	var read2 Json
	tt.Ok(cfg2.Read(&read2))

	tt.Equals(save, &read2)
}

func TestApp(t *testing.T) {

	/*go func() {
		time.Sleep(3 * time.Second)
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)
		<-c
		t.Logf("got signal")
	}()*/

	ctx, cancel := InterruptContext()
	defer func() {
		cancel()
		t.Logf("bye")
	}()
	for {
		select {
		case <- ctx.Done():
			return
		}
	}
}


