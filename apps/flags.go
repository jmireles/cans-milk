package apps

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/jmireles/cans-milk/json-rpc"
	"gitlab.com/jmireles/cans-milk/rec"
	"gitlab.com/jmireles/cans-milk/velos"
	"gitlab.com/jmireles/cans-milk/velos/server"
)

type F struct {
	*flag.FlagSet
	params []string
}

func NewF() *F {
	// No parameters
	if len(os.Args) <= 1 {
		return nil
	}
	// Second parameters and optional extra
	name, params := os.Args[1], os.Args[2:]

	return &F{
		FlagSet: flag.NewFlagSet(name, flag.ExitOnError),
		params:  params,
	}
}

func (f *F) Done() {
	f.Parse(f.params)
}

func FlagsHistory(f *F) {
	name := history(f)
	cfgs, err := newCfgs(*name, "v0.0.0", nil)
	if err != nil {
		Exit(err)
		return
	}
	ctx, cancel := InterruptContext()
	defer cancel()
	Exit(rec.NewHistory(ctx, cfgs.name, cfgs.version))
}

func FlagsReverse(f *F) {

	port, target := reverseProxy(f)
	if *port < 1024 || *port > 0xffff {
		Exit(fmt.Errorf("Invalid port: %d", *port))
		return
	}
	if *target == "" {
		Exit(fmt.Errorf("Invalid target: %s", *target))
		return
	}
	Exit(ReverseListenAndServe(*port, *target))
}

func FlagsVelosClient(f *F) {

	cfg, err := velosClient(f)
	if err != nil {
		Exit(err)
		return
	}
	ctx, cancel := InterruptContext()
	defer cancel()
	Exit(velos.NewSimClient(ctx, cfg))
}

func FlagsVelosServer(f *F) {

	port, user, pass, log := velosSrv(f)
	auth := rpc.NewAuth(*user, *pass)

	cfg := server.NewCfg(*port, auth, *log)
	ctx, cancel := InterruptContext()
	defer cancel()
	Exit(velos.NewServer(ctx, cfg))
}

func history(f *F) (name *string) {
	defer f.Done()
	name = f.String("name", "cans-dongle", "The app's name.")
	return
}

func reverseProxy(f *F) (port *int, target *string) {
	defer f.Done()
	port = f.Int("port", 8080, "reverse proxy port")
	target = f.String("target", "???", "The backend target")
	return
}

func velosClient(f *F) (cfg *velos.Cfg, err error) {
	file := f.String("file", "", "The velos configuration params.")
	url  := f.String("url", "", "The URL, if other than empty overrides file's.")
	user := f.String("user", "", "The username, if other than empty overrides file's.")
	pass := f.String("pass", "", "The password, if other than empty overrides file's.")
	f.Done()
	if *file != "" {
		cfg, err = velos.NewCfgFile(*file)
		if err != nil {
			return
		}
	} else {
		cfg = velos.CfgDefault()
	}
	if *url != "" {
		cfg.URL = *url
	}
	if *user != "" {
		cfg.User = *user
	}
	if *pass != "" {
		cfg.Pass = *pass
	}
	return
}

func velosSrv(f *F) (port *int, user *string, pass *string, log *int) {
	defer f.Done()
	port = f.Int("port", 8080, "Velos server port")
	user = f.String("user", "u53r", "Velos server authentication username")
	pass = f.String("pass", "p455", "Velos server authentication password")
	log  = f.Int("log", 2, "Velos server logging level: 0=no, 1=some, 2=all")
	return
}




