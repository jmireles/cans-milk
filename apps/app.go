package apps

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"sort"
	"strings"

	"gitlab.com/jmireles/cans-base"
)

type App interface {

	// Cfgs returns a GUI console to configure parameters for this app run
	Cfg(ctx context.Context) error

	// Run runs app reading cfg, writes records, 
	Run(ctx context.Context) error

	// History returns a GUI console to inspect records written by App's run
	History(ctx context.Context) error
}

func InterruptContext() (context.Context, func()) {
	return signal.NotifyContext(context.Background(), os.Interrupt)
}

func Usage(name string, options map[string]string) {
	max := 0
	keys := make([]string, 0, len(options))
	for k := range options {
		if max < len(k) {
			max = len(k)
		}
		keys = append(keys, k)
	}
	sort.Strings(keys)
	fmt.Printf("\nUsage of %s%s%s:\n", base.Green, name, base.Reset)
	for _, k := range keys {
		fmt.Printf("  %s%s%s%s %s\n", base.Green, k, base.Reset, strings.Repeat(" ", max - len(k)), options[k])
	}
	Exit(nil)
}

func Exit(err error) {
	if err != nil {
		fmt.Printf("%s%v%s\n", base.Red, err, base.Reset)
		os.Exit(1)
	} else {
		os.Exit(0)
	}
}


