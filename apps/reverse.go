package apps

import (
    "errors"
    "fmt"
    "net/http"
    "net/http/httputil"
    "net/url"

    "gitlab.com/jmireles/cans-base"
)

// NewProxy takes target host and creates a reverse proxy
func NewProxy(targetHost string) (*httputil.ReverseProxy, error) {
    url, err := url.Parse(targetHost)
    if err != nil {
        return nil, err
    }

    proxy := httputil.NewSingleHostReverseProxy(url)

    originalDirector := proxy.Director
    proxy.Director = func(req *http.Request) {
        originalDirector(req)
        //modifyRequest(req)
    }

    //proxy.ModifyResponse = modifyResponse()
    proxy.ErrorHandler = errorHandler()
    return proxy, nil
}

func modifyRequest(req *http.Request) {
    req.Header.Set("X-Proxy", "Simple-Reverse-Proxy")
}

func errorHandler() func(http.ResponseWriter, *http.Request, error) {
    return func(w http.ResponseWriter, req *http.Request, err error) {
        fmt.Printf("Got error while modifying response: %v \n", err)
        return
    }
}

func modifyResponse() func(*http.Response) error {
    return func(resp *http.Response) error {
        return errors.New("response body is invalid")
    }
}

// ProxyRequestHandler handles the http request using proxy
func ProxyRequestHandler(proxy *httputil.ReverseProxy) func(http.ResponseWriter, *http.Request) {
    return func(w http.ResponseWriter, r *http.Request) {
        proxy.ServeHTTP(w, r)
    }
}

func ReverseListenAndServe(port int, target string) error {
    // initialize a reverse proxy and pass the actual backend server url here
    proxy, err := NewProxy(target)
    if err != nil {
        return err
    }
    fmt.Printf("%sProxy at port=%d target=%s%s\n", base.Green, port, target, base.Reset)

    // handle all requests to your server using the proxy
    http.HandleFunc("/", ProxyRequestHandler(proxy))

    return http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
}