package apps

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
)

var bytes = []byte{
	35, 46, 57, 24, 
	85, 35, 24, 74, 
	87, 35, 88, 98, 
	66, 32, 14, 05,
}

type AESGarbler struct {
	secret []byte
}

func NewAESGarbler(secret []byte) *AESGarbler {
	return &AESGarbler{
		secret: secret,
	}
}

func (g *AESGarbler) Save(text []byte) ([]byte, error) {
	block, err := aes.NewCipher(g.secret)
	if err != nil {
		return nil, err
	}
	cfb := cipher.NewCFBEncrypter(block, bytes)
	cipherText := make([]byte, len(text))
	cfb.XORKeyStream(cipherText, text)
	return []byte(base64.StdEncoding.EncodeToString(cipherText)), nil
}

func (g *AESGarbler) Read(text []byte) ([]byte, error) {
	block, err := aes.NewCipher([]byte(g.secret))
	if err != nil {
		return nil, err
	}
	cipherText, err := base64.StdEncoding.DecodeString(string(text))
	if err != nil {
		return nil, err
	}
	cfb := cipher.NewCFBDecrypter(block, bytes)
	out := make([]byte, len(cipherText))
	cfb.XORKeyStream(out, cipherText)
	return out, nil
}

