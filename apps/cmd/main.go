package main
	
import (
	"gitlab.com/jmireles/cans-milk/apps"
)

// kill %% (kill most recent job)

func main() {

	f := apps.NewF()
	if f == nil {
		usage()
		return
	}

	switch f.Name() {
	
	case "gui":
		// TERM=linux
		apps.NewGui()

	case "history":
		apps.FlagsHistory(f)

	case "reverse":
		// arg = reverse: Run reverse proxy reading reverse's flags
		apps.FlagsReverse(f)

	case "velos-client":
		apps.FlagsVelosClient(f)

	case "velos-server":
		apps.FlagsVelosServer(f)

	default:
		usage()
	}
}

func usage() {
	apps.Usage("cans-milk", map[string]string{
		"-h":              "Prints this help and exit.",
		"gui":             "Gui for cans-milk cfgs.",
		"history":         "History for app.",
		"history -h":      "History params help.",
		"reverse":         "Reverse proxy.",
		"reverse -h":      "Reverse proxy params help.",
		"velos-client":    "Velos client to test velos server.",
		"velos-client -h": "Velos client params help.",
		"velos-server":    "Velos server to test velos clients.",
		"velos-server -h": "Velos server params help.",
	})
}
