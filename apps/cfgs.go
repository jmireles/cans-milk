package apps

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path/filepath"

	"github.com/rivo/tview"

	"gitlab.com/jmireles/cans-milk/boumatic"
	"gitlab.com/jmireles/cans-milk/term"
)

type Cfgs struct {
	name    string
	version string
	config  string
	garbler Garbler
}

func NewCfgs(name, version, private string) (*Cfgs, error) {

	if private == "" {
		return newCfgs(name, version, nil)
	}

	garbler := NewAESGarbler([]byte(private))
	return newCfgs(name, version, garbler)
}

func newCfgs(name, version string, garbler Garbler) (*Cfgs, error) {
	config, err := boumatic.UserConfig()
	if err != nil {
		return nil, err
	}
	return &Cfgs{
		name:    name,
		version: version,
		config:  config,
		garbler: garbler,
	}, nil
}

func (c *Cfgs) Name() string {
	return c.name
}

func (c *Cfgs) Version() string {
	return c.version
}

func (c *Cfgs) path() string {
	return filepath.Join(c.config, c.name + ".cfg")
}

func (c *Cfgs) Read(file any) error {
	bytes, err := ioutil.ReadFile(c.path())
	if err != nil {
		return err
	}
	if c.garbler != nil {
		if b, err := c.garbler.Read(bytes); err != nil {
			return err
		} else {
			bytes = b
		}
	}
	return json.Unmarshal(bytes, file)
}

func (c *Cfgs) Save(file any) error {
	bytes, err := json.Marshal(file)
	if err != nil {
		return err
	}
	if c.garbler != nil {
		if b, err := c.garbler.Save(bytes); err != nil {
			return err
		} else {
			bytes = b
		}
	}
	return ioutil.WriteFile(c.path(), bytes, 0644)
}

func (c *Cfgs) Header() *tview.TextView {
	header := tview.NewTextView().
		SetTextAlign(tview.AlignCenter).
		SetDynamicColors(true)
	fmt.Fprintf(header, "[white]Configurator for [green]%s[white] version [yellow]%s",
		c.name, c.version)
	return header
}

func (c *Cfgs) Help(h *term.Help) {

	h.Print2W("Help (this page).")

	h.Print4BG("Save",
		`Click to save configuration to file:
		[yellow]` + c.path())

	h.Print4BG("Quit",
		`Click to exit the configurator.`)
}

type Garbler interface {

	Save([] byte) ([]byte, error)

	Read([] byte) ([]byte, error)
} 


