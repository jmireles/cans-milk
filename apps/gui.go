package apps

import (
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"

	"gitlab.com/jmireles/cans-milk/cert"
	"gitlab.com/jmireles/cans-milk/mqtt"
	"gitlab.com/jmireles/cans-milk/p19"
	"gitlab.com/jmireles/cans-milk/rec"
	"gitlab.com/jmireles/cans-milk/term"
	"gitlab.com/jmireles/cans-milk/velos"
	"gitlab.com/jmireles/cans-milk/velos/server"
)

func NewGui() {

	cfg, err := NewCfgs("cans-milk", "0.0.0", "")
	if err != nil {
		Exit(err)
		return
	}

	var file *GuiFile
	if err = cfg.Read(&file); err != nil {
		file = &GuiFile{}
	}
	v := &Gui{
		Term:     term.NewTerm(),
		GuiFile: file,
	}
	Exit(v.RunGrid(tview.NewGrid().
		SetRows(1, 0).
		SetColumns(31, 0).
		AddItem(cfg.Header(),    0, 0, 1,  2, 0, 0, false).
		AddItem(v.controls(cfg), 1, 0, 1,  1, 0, 0, false).
		AddItem(v.Center(),      1, 1, 1,  1, 0, 0, false)))
}

type GuiFile struct {
	Cert     *cert.Cfg   `json:"cert,omitempty"`
	Mqtt     *mqtt.Cfg   `json:"mqtt,omitempty"`
	P19      *p19.Cfg    `json:"p19,omitempty"`
	Recs     *rec.Cfg    `json:"recs,omitempty"`
	Velos    *velos.Cfg  `json:"velos,omitempty"`
	VelosSrv *server.Cfg `json:"velos_srv,omitempty"`
}

type Gui struct {

	*term.Term
	*GuiFile
}

func (v *Gui) controls(c *Cfgs) *tview.Flex {
	list := tview.NewList().
		AddItem("Certificates",   "", 'c', v.cert).
		AddItem("MQTT broker",    "", 'm', v.mqtt).
		AddItem("P19 TCP server", "", 'p', v.p19).
		AddItem("Recs writer",    "", 'r', v.recs).
		AddItem("Velos client",   "", 'v', v.velos).
		AddItem("Velos server",   "", 's', v.velosSrv).
		AddItem("Help",           "", 'h', func(){ v.help(c) })

	v.cert()
	return tview.NewFlex().
		SetDirection(tview.FlexRow).
		AddItem(list, 7*2, 1, false).
		AddItem(tview.NewForm().
			AddButton("Save", func(){
				v.ModalYesNo("Save?", tcell.ColorGreen, tcell.ColorBlack, func() {
					if err := c.Save(v.GuiFile); err != nil {
						v.Modal("", err)
					}
				})
			}).
			AddButton("Quit", v.Quit), 0, 1, false)
}

func (v *Gui) cert() {
	v.FormEnabled("Cert", 
		func() bool { return v.Cert == nil },
		func(enable bool) {
			if enable {
				v.Cert = &cert.Cfg{}
			} else {
				v.Cert = nil
			}
		},
		v.cert, // refresh
		func(form *tview.Form) {
			v.Cert.Form(form, v.Center(), v.cert) // refresh
		})
}

func (v *Gui) mqtt() {
	v.FormEnabled("MQTT",
		func() bool { return v.Mqtt == nil },
		func(enable bool) {
			if enable {
				v.Mqtt = mqtt.CfgDefault()
			} else {
				v.Mqtt = nil
			}
		},
		v.mqtt, // refresh
		v.Mqtt.Form)
}

func (v *Gui) p19() {
	v.FormEnabled("P19",
		func() bool { return v.P19 == nil },
		func(enable bool) {
			if enable {
				v.P19 = p19.CfgDefault()
			} else {
				v.P19 = nil
			}
		},
		v.p19, // refresh
		v.P19.Form)
}

func (v *Gui) recs() {
	v.FormEnabled("Recs",
		func() bool { return v.Recs == nil },
		func(enable bool) {
			if enable {
				v.Recs = rec.CfgDefault()
			} else {
				v.Recs = nil
			}
		},
		v.recs, // refresh
		v.Recs.Form)
}

func (v *Gui) velos() {
	v.FormEnabled("Velos Client",
		func() bool { return v.Velos == nil },
		func(enable bool) {
			if enable {
				v.Velos = velos.CfgDefault()
			} else {
				v.Velos = nil
			}
		},
		v.velos, // refresh
		v.Velos.Form)
}

func (v *Gui) velosSrv() {
	v.FormEnabled("Velos Server",
		func() bool { return v.VelosSrv == nil },
		func(enable bool) {
			if enable {
				v.VelosSrv = server.CfgDefault()
			} else {
				v.VelosSrv = nil
			}
		},
		v.velosSrv, // refresh
		v.VelosSrv.Form)
}

func (v *Gui) help(c *Cfgs) {
	h := term.NewHelp()

	cert.CfgHelp(h)
	mqtt.CfgHelp(h)
	p19.CfgHelp(h)
	rec.CfgHelp(h)
	velos.CfgHelp(h)
	server.CfgHelp(h)
	c.Help(h)
	v.CenterSet(h, " Help ")
}

