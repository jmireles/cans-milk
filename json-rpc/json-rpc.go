package rpc

import (
	"encoding/json"
	"fmt"
	"io"
	"strings"

	"gitlab.com/jmireles/cans-base"
)

type Method string

// https://www.jsonrpc.org/specification

type Req struct {
	Jsonrpc string  `json:"jsonrpc"` // should be 2.0
	Id      float64 `json:"id"`
	Method  string  `json:"method"`
	Params  any     `json:"params"`
}

func NewJsonReq(id int, method Method, params any) *Req {
	return &Req{
		Jsonrpc: "2.0",
		Id:      float64(id),
		Method:  string(method),
		Params:  params,  
	}
}

func (r *Req) Bytes() ([]byte, error) {
	return json.Marshal(r)
}

func (r *Req) String() string {
	b, _ := json.Marshal(r)
	return string(b)
}

func (r *Req) Debug() string {
	if r == nil {
		return fmt.Sprintf("%sNone%s", 
			base.Yellow, base.Reset)
	}
	if r.Jsonrpc != "2.0" {
		return fmt.Sprintf("%sInvalid jsonrpc:%s%s", 
			base.Red, r.Jsonrpc, base.Reset)	
	}
	return fmt.Sprintf("%sid=%d method=%v params=%v%s", 
		base.Green, int(r.Id), r.Method, r.Params, base.Reset)
}

func (r *Req) Syslog(sb *strings.Builder) {
	if r == nil {
		sb.WriteString(" [No-Req]")
	} else {
		sb.WriteString(fmt.Sprintf(" (%d)", int(r.Id)))
	}
}





type Resp struct {
	Jsonrpc string      `json:"jsonrpc"`          // should be 2.0
	Id      float64     `json:"id"`               // IMPORTANT don't set omitempty
	Result  interface{} `json:"result,omitempty"`
	Error   *Error      `json:"error,omitempty"`
}

func NewJsonResp(body io.Reader) (*Resp, error) {
	var resp *Resp
	if err := json.NewDecoder(body).Decode(&resp); err != nil {
		return nil , fmt.Errorf("Invalid JSON")
	}
	if v := resp.Jsonrpc; v != "2.0" {
		return resp, fmt.Errorf("Invalid json-rpc version:%s", v)
	}
	return resp, nil
}

func (r *Resp) String() string {
	b, _ := json.Marshal(r)
	return string(b)
}

func (r *Resp) Debug() string {
	if r == nil {
		return fmt.Sprintf("%sNone%s", 
			base.Yellow, base.Reset)
	}
	if r.Error != nil {
		return fmt.Sprintf("%sid=%d err(%s)%s",
			base.Red, int(r.Id), r.Error.Debug(), base.Reset)
	}
	return fmt.Sprintf("%sid=%d result(%v)%s",
			base.Blue, int(r.Id), r.Result, base.Reset)
}

func (r *Resp) Syslog(sb *strings.Builder) {
	if r == nil {
		sb.WriteString(" [No-Resp]")
	} else {
		sb.WriteString(fmt.Sprintf(" (%d)", int(r.Id)))
		if v := r.Result; v != nil {
			sb.WriteString(fmt.Sprintf(" <%v>", v))
		}
		if e := r.Error; e != nil {
			sb.WriteString(fmt.Sprintf(" [%q]", e.Message))
		}
	}
}

type Error struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Data    any    `json:"data,omitempty"`
}

func (e *Error) Debug() string {
	return fmt.Sprintf("code=%d message=%v data=%v",
		e.Code, e.Message, e.Data)
}

func (err *Error) resp(req *Req) *Resp {
	if req == nil {
		return &Resp{
			Jsonrpc: "2.0",
			Error:   err,
		}
	}
	return &Resp{
		Jsonrpc: req.Jsonrpc,
		Id:      req.Id,
		Error:   err,
	}
}

// Invalid JSON was received by the server.
// An error occurred on the server while parsing the JSON text.
func NewRespParseError() *Resp {
	return (&Error{
		Code:    -32700,
		Message: "parse error",
	}).resp(nil)
}

// The JSON sent is not a valid Request object.
func NewRespInvalidRequest() *Resp {
	return (&Error{
		Code:    -32600,
		Message: "invalid request",
	}).resp(nil)
}

func (r *Resp) Bytes() ([]byte, error) {
	return json.Marshal(&r)
}

// NewReq parses the body for a request.
// Returns request nil for two errors: parse-error and invalid-request
// Both errors responses are returned to be send to client
// Returns request no nil to be inspected for id,method,params.
func NewReq(body io.ReadCloser) *Req {
	if body == nil {
		return nil
	}
	defer body.Close()
	var req *Req
	if err := json.NewDecoder(body).Decode(&req); err != nil {
		return nil
	}
	return req
}

func NewRespOk(req *Req, result any) *Resp {
	return &Resp{
		Jsonrpc: req.Jsonrpc,
		Id:      req.Id,
		Result:  result,
	}
}

// This is mehtod is custom
func NewRespError(req *Req, err error) *Resp {
	e := &Error{
		Code:    -32000,
		Message: err.Error(),
	}
	return e.resp(req)
}

// The method does not exist / is not available.
func NewRespMethodNotFound(req *Req) *Resp {
	e := &Error{
		Code:    -32601,
		Message: "Method not implemented",
	}
	return e.resp(req)
}

// Invalid method parameter(s).
func NewRespInvalidParams(req *Req) *Resp {
	e := &Error {
		Code:    -32602,
		Message: "Invalid parameters",
	}
	return e.resp(req)
}

// Internal JSON-RPC error.
func NewRespInternalError(req *Req) *Resp {
	e := &Error {
		Code:    -32603,
		Message: "Internal error",
	}
	return e.resp(req)
}

