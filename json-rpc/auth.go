package rpc

import (
	"net/http"
	"time"
)

type Auth struct {
	user string
	pass string

	sessions []string
}

func NewAuth(user, pass string) *Auth {
	return &Auth{
		user:     user,
		pass:     pass,

		sessions: make([]string, 0),
	}
}

func (a *Auth) User() string {
	return a.user
}

func (a *Auth) Pass() string {
	return a.pass
}

// LoginOk returns true if request BasicAuth matched this authenticator.
// a session is writer header but not code 200 is written yet.
// Returns false in authentication failure, write session to "" but
// no code 401 is written yet.
func (a *Auth) LoginOk(w http.ResponseWriter, r *http.Request) bool {
	user, pass, ok := r.BasicAuth()

	if a.user == user && a.pass == pass && ok {
		// Login success
		session := a.newSession()
		w.Header().Add("Set-Cookie", session)
		return true
	} else {
		// Login error
		w.Header().Add("Set-Cookie", "") // clear session
		return false
	}
}

func (a *Auth) LogoutOk(w http.ResponseWriter, r *http.Request) bool {
	session := r.Header.Get("Cookie")
	if a.validSession(session) {
		// session previosly send is OK
		i := 0 // output index
		for _, s := range a.sessions {
		    if session != s {
		        a.sessions[i] = s
		        i++
		    }
		}
		a.sessions = a.sessions[:i]

		w.Header().Add("Set-Cookie", "")
		return true
	}
	return false
}

func (a *Auth) SessionOk(w http.ResponseWriter, r *http.Request) bool {

	// check basic auth to accept and set session also
	user, pass, ok := r.BasicAuth()
	if a.user == user && a.pass == pass && ok {
		w.Header().Add("Set-Cookie", a.newSession())
		return true
	}

	// First check session
	session := r.Header.Get("Cookie")
	if a.validSession(session) {
		// session previosly send is OK
		return true
	}
	w.Header().Add("Set-Cookie", "") // clear session
	return false
}

func (a *Auth) newSession() string {
	s := time.Now().Format(time.RFC3339Nano)
	a.sessions = append(a.sessions, s)
	return s
}

func (a *Auth) validSession(session string) bool {
	for _, s := range a.sessions {
		if s == session {
			return true
		}
	}
	return false
}
