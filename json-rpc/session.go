package rpc

import (
	"bytes"
	"fmt"
	"net/http"
)

type Session struct {
	url    string
	user   string
	pass   string
	log    Log
	id     int
	cookie string
}

func NewSession(url, user, pass string, log Log) *Session {
	return &Session{
		url:    url,
		user:   user,
		pass:   pass,
		log:    log,
		id:     1,
		cookie: "",
	}
}

func (s *Session) Init() {
	s.id = 1
	s.cookie = ""
}

func (s *Session) Post(path string, method Method, params any) *Call {


	call := &Call{
		Post: path,
	}
	// Report response
	defer s.log(call)

	call.Req = NewJsonReq(s.id, method, params)
	req, err := call.Req.Bytes()
	if err != nil {
		return call.SetError("req json:%v", err)
	}

	url := fmt.Sprintf("%s%s", s.url, path)
	post, err := http.NewRequest("POST", url, bytes.NewBuffer(req))
	if err != nil {
		return call.SetError("request:%v", err)
	}
	
	post.Header.Add("Content-Type", "application/json-rpc")
	post.Header.Add("Accept", "application/json-rpc")

	if s.cookie == "" {
		// At the begining, after a 401 response or after a logout
		// c.Session is set to "", set basic auth to notify server
		// we want a new Session
		post.SetBasicAuth(s.user, s.pass)
	} else {
		// Set the current Session for server validation
		post.Header.Add("Cookie", s.cookie)
	}

	client := &http.Client{}
	r, err := client.Do(post)
	
	if err != nil {
		return call.SetError("post error:%v", err)
	}
	if cookie, ok := r.Header["Set-Cookie"]; ok || len(cookie) > 0 {
		call.Session = cookie[0] // for debugging
		s.cookie = cookie[0]    // for next calls
	}
	
	call.Code = r.StatusCode // 200,400,401,404,500
	resp, err := NewJsonResp(r.Body)
	call.Resp = resp
	if err != nil {
		return call.SetError("rpc resp error:%v", err)
	}

	switch r.StatusCode {

	case http.StatusOK:
		if id := int(resp.Id); s.id != id {
			return call.SetError("Different: c.Id=%d resp.Id=%d", s.id, id)

		} else {
			s.id++
		}

	case http.StatusBadRequest: // 400
		return call.SetError("Bad request")

	case http.StatusUnauthorized: // 401
		return call.SetError("Unauthorized")

	case http.StatusNotFound: // 404	
		return call.SetError("Not found")
	
	case http.StatusInternalServerError: // 500
		return call.SetError("Server error")	

	case http.StatusBadGateway: // 502
		return call.SetError("Bad gateway")	

	default:
		return call.SetError("code:%d", r.StatusCode)	
	}

	return call
}
