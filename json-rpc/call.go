package rpc

import (
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"gitlab.com/jmireles/cans-base"
)

type Log func(call *Call)

type call struct {
	Req  *Req
	Resp *Resp
}


// Post is the url api
// Req is the RPC request
// Session is different of "" if server sends a new session
// Code is the HTTP response code
// Resp is the RPC response
// Error describes an error for request or response
type Call struct {
	call
	Post    string
	Code    int
	Session string
	Error   error
}

// Syslog returns a string with:
//	1 - The request (id) or No-Req
//  2 - The response code 200,401,500...
//	3 - The response (id) and <result> if and {error.Message} if any
func (c *Call) Syslog() string {
	if c == nil {
		return " [Empty call]"
	}
	var sb strings.Builder
	c.Req.Syslog(&sb)
	sb.WriteString(fmt.Sprintf(" >> %d", c.Code))
	c.Resp.Syslog(&sb)
	return sb.String()
}

// error sets the Error of this call and returns the call
func (c *Call) SetError(f string, args ...any) *Call {
	c.Error = fmt.Errorf(f, args...)
	return c
}

func (c *Call) Debug() {
	if c == nil {
		return
	}
	err := ""
	if c.Error != nil {
		err = c.Error.Error()
	}
	fmt.Printf("Call:\n")
	fmt.Printf(" ├─ Req:  %v %s\n", c.Post, c.Req.Debug())
	fmt.Printf(" └─ Resp: %d %s %s %s%s%s\n", 
		c.Code, err, c.Resp.Debug(), 
		base.Yellow, c.Session, base.Reset)
}

func (c *Call) Metrics(w io.Writer) {
	fmt.Fprintf(w, `"post":%q`, c.Post)
	if r := c.Req; r != nil {
		fmt.Fprintf(w, `,"req":%v`, r)
	}
	fmt.Fprintf(w, `,"code":%d`, c.Code)
	if s := c.Session; s != "" {
		fmt.Fprintf(w, `,"session":%s`, s)
	}
	if r := c.Resp; r != nil {
		fmt.Fprintf(w, `,"resp":%v`, c.Resp)
	}
	if e := c.Error; e != nil {
		fmt.Fprintf(w, `,"error":%q`, c.Error.Error())
	}
}

func (c *Call) Rows(r1, r2, r3, r4 io.Writer) {
	// row 1
	fmt.Fprintf(r1, "[white]POST %s", c.Post)
	if r := c.Req; r == nil {
		fmt.Fprintf(r1, " [yellow]None")
	} else if r.Jsonrpc != "2.0" {
		fmt.Fprintf(r1, " [red]Invalid jsonrpc:%s", r.Jsonrpc)
	} else {
		fmt.Fprintf(r1, " [white]id=[green]%d", int(r.Id))
		fmt.Fprintf(r1, " [white]m=[green]%v", r.Method)
		fmt.Fprintf(r1, " [white]p=[green]%v", r.Params)
	}
	// row 2
	fmt.Fprintf(r2, "[white]RESP %d", c.Code)
	if session := c.Session; session != "" {
		fmt.Fprintf(r2, " [yellow]%s", session)
	}
	if err := c.Error; err != nil {
		fmt.Fprintf(r2, " [red]%v", err)
	}
	if r := c.Resp; r != nil {
		if r.Jsonrpc != "2.0" {
			fmt.Fprintf(r2, " [red]Invalid jsonrpc:%s", r.Jsonrpc)
		} else {
			fmt.Fprintf(r2, " [white]id=[green]%d", int(r.Id))
			if r.Result != nil {
				fmt.Fprintf(r2, " [white]id=[green]%v", r.Result)
			}
			if e := r.Error; e != nil {
				fmt.Fprintf(r2, " [white]err.code=[red]%d", e.Code)
				// rows 3,4
				rows := strings.Split(e.Message, "\n")
				if len(rows) > 0 {
					fmt.Fprintf(r3, "[yellow]%s", rows[0])
				}
				if len(rows) > 1 {
					fmt.Fprintf(r4, "[yellow]%s", rows[1])
				}
			}
		}
	}
}

type CallServer struct {
	call
	w   http.ResponseWriter
	r   *http.Request	
	log int
}

func NewCallServer(w http.ResponseWriter, r *http.Request, log int) *CallServer {
	return &CallServer{
		w:   w,
		r:   r,
		log: log,
	}	
}

func (c *CallServer) Method() Method {
	if c.Req == nil {
		return Method("")
	}
	return Method(c.Req.Method)
}

func (c *CallServer) RequestOk() bool {
	if req := NewReq(c.r.Body); req == nil {
		return false
	} else {
		c.Req = req
		return true
	}
}

func (c *CallServer) LoginOk(auth *Auth) bool {
	if auth == nil {
		return false
	}
	return auth.LoginOk(c.w, c.r)
}

func (c *CallServer) LogoutOk(auth *Auth) bool {
	if auth == nil {
		return false
	}
	return auth.LogoutOk(c.w, c.r)
}

func (c *CallServer) SessionOk(auth *Auth) bool {
	if auth == nil {
		return false
	}
	return auth.SessionOk(c.w, c.r)
}

func (c *CallServer) WriteOk() {
	c.write(200, NewRespOk(c.Req, nil))
}

func (c *CallServer) WriteNoAuth() {
	c.write(401, NewRespOk(c.Req, nil))
}

func (c *CallServer) WriteNoMethod() {
	c.write(404, NewRespMethodNotFound(c.Req))
}

func (c *CallServer) WriteInvalidParams() {
	c.write(404, NewRespInvalidParams(c.Req))
}

func (c *CallServer) WriteInternalError() {
	c.write(500, NewRespInternalError(c.Req))
}

func (c *CallServer) write(code int, resp *Resp) {

fmt.Println("write", code, resp)
	c.Resp = resp
	c.w.Header().Set("Content-Type", "application/json")
	c.w.WriteHeader(code)		
	if bytes, err := resp.Bytes(); err != nil {
		c.w.Write([]byte("{}"))
	} else {
		c.w.Write(bytes)
	}
	if c.log > 0 {
		c.logs(code)
	}
}

func (c *CallServer) logs(code int) {
	post := c.r.URL.Path
	if a1 := c.r.Header.Get("Authorization"); a1 != "" {
		// print client basic auth
		post = fmt.Sprintf("%s %sauth=%s%s", post, base.Yellow, a1, base.Reset)
	}
	if s1 := c.r.Header.Get("Cookie"); s1 != "" {
		// print client's session
		post = fmt.Sprintf("%s %ssession=%s%s", post, base.Yellow, s1, base.Reset)
	}

	// new session to client
	s2 := c.w.Header().Get("Set-Cookie")

	now := time.Now()
	fmt.Printf("Velos server post: %s %s\n  %s%v%s\n  %s%d %v%s %s%s\n", 
		now.Format("15:04:05"), post,
		base.Green, c.Req, base.Reset,
		base.Blue, code, c.Resp, base.Yellow, s2, base.Reset)
}


