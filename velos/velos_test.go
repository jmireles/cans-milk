package velos

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"path/filepath"
	"syscall"
	"testing"
	"time"

	"gitlab.com/jmireles/cans-base"

	"gitlab.com/jmireles/cans-milk/json-rpc"
	"gitlab.com/jmireles/cans-milk/velos/server"
)

var (
	u1, p1 = "userName", "password"
	u2, p2 = "username", "password"
	auth1 = rpc.NewAuth(u1, p1)
	auth2 = rpc.NewAuth(u2, p2)
	cli1 = &Cfg{
		URL:  "http://localhost:8080",
		User: u1,
		Pass: p2,
	}
	cli2 = &Cfg{
		URL:  "http://localhost:8080",
		User: u2,
		Pass: p2,
	}
)

func TestCfg(t *testing.T) {

	tt := base.Test{ T:t }
	f, err := ioutil.TempFile("", "velos.json")
	tt.Ok(err)
	defer syscall.Unlink(f.Name())
	
	// write
	ioutil.WriteFile(f.Name(), []byte(`{
		"url":  "http://localhost:8080",
		"user": "userName",
		"pass": "password"
	}`), 0777)
	// read
	cfg, err := NewCfgFile(f.Name())
	tt.Ok(err)
	tt.Equals("http://localhost:8080", cfg.URL)
	tt.Equals(auth1, rpc.NewAuth(cfg.User, cfg.Pass))
	t.Log(f.Name(), cfg)
}

func TestMetrics(t *testing.T) {

	tt := base.Test{ T:t }

	m := NewMetrics("test")

	post := Smartflow.String()
	for _, c := range []struct {
		req   *rpc.Req
		code   int
		resp   string
		err    error
		metric string
	} {
		{
			// Velos server cannot accept the start because the place has no float associated
			req:    rpc.NewJsonReq(2, StartMeasuring, []any{ 10, true }), 
			code:   500,
			resp:   "test/500-no-float.json",
			err:    fmt.Errorf("Server error"),
			metric: `{"name":"test","gets":0,"starts":1,"stops":0,"errors":1,"last-call":{"post":"/api/smartflow",`+
			`"req":{"jsonrpc":"2.0","id":2,"method":"start_measuring","params":[10,true]},"code":500,`+
			`"resp":{"jsonrpc":"2.0","id":2,"error":{"code":-32603,`+
			`"message":"Failed to notify one or more listeners:\n\tServiceException: `+
			`No float is associated with milk place 10, or the milk place is not found in any parlor."}},`+
			`"error":"Server error"}}`,
		}, {
			// Velos server accepts the start
			req:    rpc.NewJsonReq(2, StartMeasuring, []any{ 10, true }), 
			code:   200,
			resp:   "test/200-started.json",
			err:    nil,
			metric: `{"name":"test","gets":0,"starts":2,"stops":0,"errors":1,"last-call":{"post":"/api/smartflow",`+
			`"req":{"jsonrpc":"2.0","id":2,"method":"start_measuring","params":[10,true]},"code":200,`+
			`"resp":{"jsonrpc":"2.0","id":2}}}`,
		}, {
			// Velos server cannot accept the start because duplicated
			req:    rpc.NewJsonReq(2, StartMeasuring, []any{ 10, true }), 
			code:   500,
			resp:   "test/500-already-started.json",
			err:    fmt.Errorf("Server error"),
			metric: `{"name":"test","gets":0,"starts":3,"stops":0,"errors":2,"last-call":{"post":"/api/smartflow",`+
			`"req":{"jsonrpc":"2.0","id":2,"method":"start_measuring","params":[10,true]},"code":500,`+
			`"resp":{"jsonrpc":"2.0","id":2,"error":{"code":-32603,`+
			`"message":"Failed to notify one or more listeners:\n\tServiceException: `+
			`Milk measurement session on milk place 10 is already started."}},`+
			`"error":"Server error"}}`,
		}, {
			// Velos server cannot accept the start with 502 error no json valid (html message)
			req:    rpc.NewJsonReq(2, StartMeasuring, []any{ 10, true }), 
			code:   502,
			resp:   "test/502.html",
			err:    fmt.Errorf("Bad Gateway"),
			metric: `{"name":"test","gets":0,"starts":4,"stops":0,"errors":3,"last-call":{"post":"/api/smartflow",`+
			`"req":{"jsonrpc":"2.0","id":2,"method":"start_measuring","params":[10,true]},"code":502,`+
			`"error":"Bad Gateway"}}`,
		}, {
			// Velos server cannot accept the start because is not available
			req:    rpc.NewJsonReq(2, StartMeasuring, []any{ 10, true }),
			code:   0,
			resp:   "",
			err:    fmt.Errorf(`post error:Post "http://localhost:8080/api/session": dial tcp 127.0.0.1:8080: connect: connection refused`),
			metric: `{"name":"test","gets":0,"starts":5,"stops":0,"errors":4,"last-call":{"post":"/api/smartflow",`+
			`"req":{"jsonrpc":"2.0","id":2,"method":"start_measuring","params":[10,true]},"code":0,`+
			`"error":"post error:Post \"http://localhost:8080/api/session\": dial tcp 127.0.0.1:8080: connect: connection refused"}}`,
		},
	} {
		var resp *rpc.Resp
		reader, _ := os.Open(c.resp) // ignore errors
		if reader != nil {
			defer reader.Close()
			resp, _ = rpc.NewJsonResp(reader)
		}
		call := &rpc.Call{
			Post:    post,
			Code:    c.code,
			Error:   c.err,
		}
		call.Req  = c.req
		call.Resp = resp
		m.Start(call)
		var buff bytes.Buffer
		m.Write(&buff)
		tt.Equals(c.metric, buff.String())
	}


}

func TestClientConnectionRefused(t *testing.T) {
	
	// Test client without a server
	
	tt := base.Test{ T:t }
	// ...Missing server
	client := NewClient(cli1, func(call *rpc.Call) {
		call.Debug()
	})
	result := client.Login()
	tt.Assert(result.Error != nil, "error:%v", result.Error)
	t.Logf("result: %v", result)
}


func testServer(ctx context.Context, port int, auth *rpc.Auth) error {
	return NewServer(ctx, server.NewCfg(port, auth, 0))
}

func TestServerCfg(t *testing.T) {

	tt := base.Test{ T:t }
	ctx, done := context.WithTimeout(context.Background(), 3 * time.Second)
	defer done()
	t.Log("Wait 3 seconds...")
	//ctx, stop := signal.NotifyContext(ctx, os.Interrupt)
	//defer stop()
	tt.Ok(NewServer(ctx, &server.Cfg{
		Addr: ":8081",
		User: "u5er",
		Pass: "p455",
	}))
}

func TestServer8080(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 2 * time.Second)
	defer cancel()
	testServer(ctx, 8080, auth1)
}

func TestServer8081(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 2 * time.Second)
	defer cancel()
	testServer(ctx, 8081, auth1)
}

func TestClientUnauthorized(t *testing.T) {
	
	// Test client with server with different auth

	tt := base.Test{ T:t }
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go testServer(ctx, 8080, auth1)
	client := NewClient(cli2, func(call *rpc.Call) {
		call.Debug()
	})
	result := client.startMeasuring(42, false)
	tt.Assert(result.Error != nil, "error:%v", result.Error)
	t.Logf("result: %v", result)
}


func TestClientOk(t *testing.T) {

	// Test client with server with matching auth

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go testServer(ctx, 8080, auth1)
	client := NewClient(cli1, func(call *rpc.Call) {
		call.Debug()
	})

	// First login
	client.Login()

	client.startMeasuring(42, false)
	client.stopMeasuring(42, Unknown)

	client.startMeasuring(43, false)
	client.stopMeasuring(43, Manual)

	client.startMeasuring(44, false)
	client.startMeasuring(44, false)

	client.stopMeasuring(45, Unknown)

	client.startMeasuring(46, false)
	client.stopMeasuring(46, Unknown)

	client.startMeasuring(99, false)

	time.Sleep(2 * time.Second)
}

func testClientReal(log rpc.Log, syslog bool) (*Client, error) {
	pwd, err := os.Getwd()
    if err != nil {
    	return nil, err
    }
    path := filepath.Join(pwd, "../../velos.json") // out of repo reach
	cfg, err := NewCfgFile(path)
	if err != nil {
		return nil, err
	}
	cfg.Syslog = syslog
	fmt.Printf("real client cfg %s%v%s\n", base.Yellow, cfg, base.Reset)
	return NewClient(cfg, log), nil
}

func TestServerReal(t *testing.T) {
	client, err := testClientReal(func(call *rpc.Call) {
		call.Debug()
	}, false)
	if err != nil {
		t.Skip(err)
	}

	logout := func() {
		fmt.Printf("Logout ")
		client.Logout()
		time.Sleep(1 * time.Second)
	}
	defer logout()

	fmt.Print("Login ")
	client.Login()
	time.Sleep(1 * time.Second)

	logout()

	fmt.Printf("GetVersion ")
	client.GetVersion()
	time.Sleep(1 * time.Second)

	//client.cfg.Reattach = true // old mode: new version returns error for start

	time.Sleep(1 * time.Second)
	client.startMeasuring(8, false)
	time.Sleep(1 * time.Second)
	client.stopMeasuring(8, Unknown)


	//client.cfg.Reattach = false // new mode: old version should return error for start

	time.Sleep(1 * time.Second)
	client.startMeasuring(8, false)
	time.Sleep(1 * time.Second)
	client.stopMeasuring(8, Unknown)

}

func TestSyslog(t *testing.T) {
	logTypes := []string{"unixgram", "unix"}
	logPaths := []string{"/dev/log", "/var/run/syslog", "/var/run/log"}
	for _, network := range logTypes {
		for _, path := range logPaths {
			conn, err := net.Dial(network, path)
			if err == nil {
				t.Log("ok", network, path, conn)
				return
			}
		}
	}
	t.Fatal("No net")
}

func TestClientSyslog(t *testing.T) {
	// local logger
	rpcLogFunc := func(call *rpc.Call) {
		call.Debug()
	}
	// force syslog in test client cfg read from file
	syslog := true
	client, err := testClientReal(rpcLogFunc, syslog)
	if err != nil {
		t.Skip(err)
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	m, err := client.NewMeasuring(ctx, func(call *rpc.Call) {
		//if last != nil {
		//	last.Debug() for browser not here!
		//}
	})
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("Name: %s%s%s", base.Yellow, m.Name(), base.Reset)

	stops := func(reason TakeoffReason) {
		for place := 1; place <= 8; place++ {
			m.Stop(place, reason)	
			time.Sleep(1 * time.Second)
		}
	}
	starts := func(reattach bool) {
		for place := 1; place <= 8; place++ {
			m.Start(place, reattach)
			time.Sleep(1 * time.Second)
		}
	}
	stops(Unknown) // sends stops
	starts(false)  // send starts
	starts(true)   // send starts (again)
	stops(Unknown) // sends stops (again)

	time.Sleep(1 * time.Second)
}
