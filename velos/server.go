package velos

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/jmireles/cans-base"

	"gitlab.com/jmireles/cans-milk/json-rpc"
	"gitlab.com/jmireles/cans-milk/velos/server"
)

// Server a simplistic real velos server
type Server struct {
	auth *rpc.Auth
	log  int
}

func NewServer(ctx context.Context, cfg *server.Cfg) error {
	fmt.Printf("Velos Server started info: %s%s%s\n", base.Green, cfg.Info(), base.Reset)
	defer fmt.Printf("Velos-Server stop\n")

	if err := cfg.Normalize(); err != nil {
		return err
	}

	s := &Server{
		auth: rpc.NewAuth(cfg.User, cfg.Pass),
		log:  cfg.Log,
	}
	server := &http.Server{
		Addr:    cfg.Addr,
		Handler: s,
	}
	done := make(chan error)
	go func() {
		ctx, cancel := context.WithCancel(ctx)
		defer cancel()
		for {
			select {
			case <-ctx.Done():
				done<- server.Shutdown(context.Background())
				return
			}
		}
	}()
	if err := server.ListenAndServe(); err != http.ErrServerClosed {
		return err
	}
	return <-done
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	c := rpc.NewCallServer(w, r, s.log)
	switch r.Method {

	case "POST":

		switch r.URL.Path {
		case Session.String():
    		s.session(c)

    	case Smartflow.String():
    		s.smartflow(c)

    	case ProcessUnit.String():
    		s.processUnit(c)

    	default: 
    		c.WriteNoMethod() // 404
    	}

    default:
   		c.WriteNoMethod() // 404
	}
}

func (s *Server) session(c *rpc.CallServer) {

	if ok := c.RequestOk(); !ok {
		c.WriteInvalidParams() // 404
	
	} else {
		switch c.Method() {

		case Login:
			if c.LoginOk(s.auth) {
				c.WriteOk() // 200
			} else {
				c.WriteNoAuth() // 401
			}

		case Logout:
			if c.LogoutOk(s.auth) {
				c.WriteOk() // 200
			} else {
				c.WriteNoAuth() // 401
			}

		default:
			c.WriteNoMethod() // 404
		}
	}
}

func (s *Server) smartflow(c *rpc.CallServer) {

	if ok := c.RequestOk(); !ok {
		c.WriteInvalidParams() // 404

	} else if !c.SessionOk(s.auth) {
		c.WriteNoAuth() // 401

	} else {
		switch c.Method() {

		case StartMeasuring:
			// TODO check req params, duplicated start... 500
			c.WriteOk() // 200
		
		case StopMeasuring:
			// TODO check req params, duplicated stop... 500
			c.WriteOk() // 200

		default:
			c.WriteNoMethod() // 404
		}
	}
}


func (s *Server) processUnit(c *rpc.CallServer) {

	if ok := c.RequestOk(); !ok {
		c.WriteInvalidParams() // 404

	} else if !c.SessionOk(s.auth) {
		c.WriteNoAuth() // 401

	} else {
		switch c.Method() {

		case GetVersion:
			// TODO check req params
			c.WriteOk() // 200

		default:
			c.WriteNoMethod() // 404
		}
	}
}

