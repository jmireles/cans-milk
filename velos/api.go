package velos

import (
	"fmt"

	"gitlab.com/jmireles/cans-milk/json-rpc"
)

type Api string

const (
	Devices     Api = "devices"
	Livestock   Api = "livestock"
	Milk        Api = "milk"
	Session     Api = "session"
	Separation  Api = "separation"
	Smartflow   Api = "smartflow"
	Positioning Api = "positioning"
	ProcessUnit Api = "processunit"
	Weighing    Api = "weighing"
)

func (a Api) String() string {
	return fmt.Sprintf("/api/%s", string(a))
}

type Method string

const (
	Login          rpc.Method = "login"
	Logout         rpc.Method = "logout"
	
	StartMeasuring rpc.Method = "start_measuring"
	StopMeasuring  rpc.Method = "stop_measuring"
	
	Clear                     rpc.Method = "clear"
	GetVersion                rpc.Method = "get_version"
	GetLastMilking            rpc.Method = "get_last_milking"
	GetMilkData               rpc.Method = "get_milk_data"
	GetMilkings               rpc.Method = "get_milkings"
	GetMilkingsForAnimal      rpc.Method = "get_milkings_for_animal"
	GetMilkingsForAnimalSince rpc.Method = "get_milkings_for_animal_since"

	GetLogicalAddresses       rpc.Method = "get_logical_addresses"
	GetAnimal                 rpc.Method = "get_animal"

	GetDailyAnimalWeightSummary rpc.Method = "get_daily_animal_weight_summary"

)




