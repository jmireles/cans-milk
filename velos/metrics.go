package velos

import (
	"fmt"
	"io"
	"sync"

	"gitlab.com/jmireles/cans-milk/json-rpc"
)

type Metrics struct {
	sync.Mutex

	name   string

	starts int64
	stops  int64
	gets   int64
	errors int64

	last   *rpc.Call
}

func NewMetrics(name string) *Metrics {
	return &Metrics{
		name: name,
	}
}

func (h *Metrics) Start(call *rpc.Call) {
	h.Lock()
	defer h.Unlock()
	h.starts++
	if call.Error != nil {
		h.errors++
	}
	h.last = call
}

func (h *Metrics) Stop(call *rpc.Call) {
	h.Lock()
	defer h.Unlock()
	h.stops++
	if call.Error != nil {
		h.errors++
	}
	h.last = call
}

func (h *Metrics) Gets(*rpc.Call) {
	h.Lock()
	defer h.Unlock()
	h.gets++
}

func (h *Metrics) Write(w io.Writer) {
	h.Lock()
	defer h.Unlock()
	fmt.Fprint(w, `{`)

	fmt.Fprintf(w, `"name":%q`, h.name)
	fmt.Fprintf(w, `,"gets":%d`, h.gets)
	fmt.Fprintf(w, `,"starts":%d`, h.starts)
	fmt.Fprintf(w, `,"stops":%d`, h.stops)
	fmt.Fprintf(w, `,"errors":%d`, h.errors)
	fmt.Fprint(w, `,"last-call":{`)
	if h.last != nil {
		h.last.Metrics(w)
	}
	fmt.Fprint(w, `}`)
	fmt.Fprint(w, `}`)
}
