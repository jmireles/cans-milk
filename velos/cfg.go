package velos

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"time"

	"github.com/rivo/tview"
	"gitlab.com/jmireles/cans-base"

	"gitlab.com/jmireles/cans-milk/term"
)

const (
	cfgHeartbeatDefault = 5 * time.Second
)

type Cfg struct {
	URL       string    `json:"url"`
	User      string    `json:"user"`
	Pass      string    `json:"pass"`
	Heartbeat base.Time `json:"heartbeat"`
	Log       int       `json:"log"`
	Syslog    bool      `json:"syslog"`
	Reattach  bool      `json:"reattach"`
}

func CfgDefault() *Cfg {
	return &Cfg{
		Heartbeat: base.Time{ cfgHeartbeatDefault },
	}
}

func NewCfgFile(filepath string) (*Cfg, error) {
	if filepath == "" {
		// return no client for velos here and no error
		return nil, nil
	}
	var cfg Cfg
	if content, err := ioutil.ReadFile(filepath); err != nil {
		return nil, fmt.Errorf("Read file error: %s\n", err)
	
	} else if err := json.Unmarshal(content, &cfg); err != nil {
		return nil, fmt.Errorf("JSON error in file: %s %v", filepath, err)
	
	}
	cfg.Normalize()
	return &cfg, nil
}

func (c *Cfg) Info() string {
	return fmt.Sprintf(`{"url":%q,"user":%q,"heartbeat":%q,"log":%d}`,
		c.URL, c.User, c.Heartbeat, c.Log,
	)
}

func (c *Cfg) Normalize() {
	if min := cfgHeartbeatDefault; c.Heartbeat.Duration < min {
		c.Heartbeat.Duration = min
	}
}

func (c *Cfg) Form(form *tview.Form) {

	if c == nil {
		return 
	}
	form.AddInputField("URL", c.URL, 40, nil, func(url string) {
		c.URL = url
	})

	form.AddInputField("Username", c.User, 30, nil, func(user string) {
		c.User = user
	}).
	AddPasswordField("Password", c.Pass, 30, '*', func(pass string) {
		c.Pass = pass
	})

	term.NewDropDown("Heartbeat",
		term.DropItemDuration(cfgHeartbeatDefault),
	).AddForm(form, term.DropItemDuration(c.Heartbeat.Duration), func(item term.DropItem) {
		c.Heartbeat.Duration = time.Duration(item.(term.DropItemDuration))
	})

	term.NewDropDown("Log",
		term.DropItemInt(0),
		term.DropItemInt(1),
		term.DropItemInt(2),
	).AddForm(form, term.DropItemInt(c.Log), func(item term.DropItem) {
		c.Log = int(item.(term.DropItemInt))
	})

	form.AddFormItem(tview.NewCheckbox().
		SetLabel("Syslog").
		SetCheckedString(" ✔ ").
		SetChecked(c.Syslog).
		SetChangedFunc(func(checked bool) {
			c.Syslog = checked
		}))

	form.AddFormItem(tview.NewCheckbox().
		SetLabel("Reattach").
		SetCheckedString(" ✔ ").
		SetChecked(c.Reattach).
		SetChangedFunc(func(reattach bool) {
			c.Reattach = reattach
		}))
}

func CfgHelp(h *term.Help) {

	h.Print2W("Velos client")

	h.Print4BG("Enable",
		`Click to enable velos client.`)

	h.Print4YG("URL",
		`Type the Velos server URL including scheme, IP and port.`)

	h.Print4YG("Username",
		`Type the username expected by velos server.`)

	h.Print4YG("Password",
		`Type the password expected by velos server.`)

	h.Print4YG("Heartbeat",
		`Select the frequency for client heartbeats.`)

	h.Print4YG("Logs",
		`Select the level for logging:
       == 0 - No logging.
       >= 1 - Web page logging.
       >= 2 - Complete logging.`)

	h.Print4YG("Syslog",
		`Enable to write start and stops measurements to unix syslog.
       See logs with:
          $sudo tail -f /var/log/syslog | grep "milk-velos"`)

	h.Print4YG("Reattach",
		`Enable to send reattach in start measurements (old mode).`)


	h.Print4BG("Disable",
		`Click to disable Velos client.`)
}
