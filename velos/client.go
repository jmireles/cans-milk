package velos

import (
	"context"
	"fmt"
	"log/syslog"
	"net/http"
	"time"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-milk/json-rpc"
)

type Client struct {
	*rpc.Session
	cfg    *Cfg
	syslog  base.Log
}

func NewClient(cfg *Cfg, log rpc.Log) *Client {
	if cfg != nil {
		cfg.Normalize()
	}
	return &Client{
		Session: rpc.NewSession(cfg.URL, cfg.User, cfg.Pass, log),
		cfg:     cfg,
	}
}


func (c *Client) NewMeasuring(ctx context.Context, log rpc.Log) (m *Measuring, err error) {

	// syslog
	var writer *syslog.Writer
	if c.cfg.Syslog {
		writer, err = unixSyslog("milk-velos")
		if err != nil {
			return
		}
		if writer != nil {
			// set local syslog where to send starts/stops
			c.syslog = func(s string, args ...any) {
				// TODO silent error
				writer.Info(fmt.Sprintf(s, args...))
			}
		}
	}

	// measuring
	name   := fmt.Sprintf("Velos %s", c.cfg.URL)
	heartbeat := c.cfg.Heartbeat.Duration

	metrics := NewMetrics(name)
	start   := make(chan *Start)
	stop    := make(chan *Stop)
	m = &Measuring{
		name:    name,
		metrics: metrics,
		start:   start,
		stop:    stop,
	}

	go func() {
		ctx, cancel := context.WithCancel(ctx)
		defer cancel()
		ticker := time.NewTicker(heartbeat)
		defer ticker.Stop()
		for {
			select {
			case s := <- start:
				call := c.StartMeasuring(s)
				metrics.Start(call)
				log(call)

			case s := <- stop:
				call := c.StopMeasuring(s)
				metrics.Stop(call)
				log(call)

			case <-ticker.C:
				// refresh velos server with "heartbeats"
				call := c.GetVersion()
				metrics.Gets(call)

			case <- ctx.Done():
				if writer != nil {
					writer.Close()
				}
				return
			}
		}
	}()
	return
}

func (s *Client) Login() *rpc.Call {
	// Clear this session to post BasicAuth
	s.Init()
	return s.post(Session, Login, nil)
}

func (s *Client) Logout() *rpc.Call {
	call := s.post(Session, Logout, nil)	
	if call.Code == http.StatusOK {
		s.Init()
	}
	return call
}

func (c *Client) StartMeasuring(start *Start) *rpc.Call {
	return c.startMeasuring(start.place, start.reattach)
}

func (c *Client) StopMeasuring(stop *Stop) *rpc.Call {
	return c.stopMeasuring(stop.place, stop.reason)
}

func (c *Client) GetVersion() *rpc.Call {
	return c.post(ProcessUnit, GetVersion, []any{
		map[string]any{
			"name":       "vpu_1", // seems is optional
			"ob_type":    "HostName",
			"ob_version": 1,
		},
	})
}

func (c *Client) startMeasuring(place int, reattach bool) *rpc.Call {

	if c.cfg.Reattach {
		// Old mode
		call := c.post(Smartflow, StartMeasuring, []any{ 
			place,
			reattach,
		})
		if c.syslog != nil {
			c.syslog("Start p:%d r:%t %s", place, reattach, call.Syslog())
		}
		return call
	} else {
		// New mode VELOS version 5.4.0.36 don't accept starts with reattach
		call := c.post(Smartflow, StartMeasuring, []any{ 
			place,
		})
		if c.syslog != nil {
			c.syslog("Start p:%d %s", place, call.Syslog())
		}
		return call
	}
}

func (c *Client) stopMeasuring(place int, reason TakeoffReason) *rpc.Call {
	call :=  c.post(Smartflow, StopMeasuring, []any{ 
		place,
		reason,
	})
	if c.syslog != nil {
		c.syslog("Stop p:%d r:%v %s", place, reason, call.Syslog())
	}
	return call
}

func (s *Client) post(api Api, method rpc.Method, params any) *rpc.Call {
	// First call
	path := api.String()
	call := s.Post(path, method, params)
	if call.Code == http.StatusUnauthorized {

		time.Sleep(1 * time.Second) // ?
		// Second and last call
		// On 401, clear id/session to post second and last call
		// forcing to login again sending BasicAuth
		s.Init()
		return s.Post(path, method, params)
	}
	return call
} 

// Copied from https://cs.opensource.google/go/go/+/refs/tags/go1.20.6:src/log/syslog/syslog_unix.go
// We add path /run/systemd/journal/dev-log to fix WRONG radxa no /dev/log linked properly.
func unixSyslog(topic string) (*syslog.Writer, error) {
	logTypes := []string{
		"unixgram",
		"unix",
	}
	logPaths := []string{
		"/dev/log",
		"/var/run/syslog",
		"/var/run/log",
		"/run/systemd/journal/dev-log", // normally this where /dev/log links to.
	}
	for _, network := range logTypes {
		for _, path := range logPaths {
			writer, err := syslog.Dial(network, path, syslog.LOG_INFO, topic)
			if err == nil {
				return writer, nil
			}
		}
	}
	return nil, fmt.Errorf("Unix syslog delivery error")
}






