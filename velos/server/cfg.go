package server

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"github.com/rivo/tview"

	"gitlab.com/jmireles/cans-milk/json-rpc"
	"gitlab.com/jmireles/cans-milk/term"
)

type Cfg struct {
	Addr string `json:"addr"`
	User string `json:"user"`
	Pass string `json:"pass"`
	Log  int    `json:"log"`
}

func NewCfg(port int, auth *rpc.Auth, log int) *Cfg {
	return &Cfg{
		Addr: fmt.Sprintf(":%d", port),
		User: auth.User(),
		Pass: auth.Pass(),
		Log:  log,
	}
}

func CfgDefault() *Cfg {
	return &Cfg{

	}
}

func NewCfgFile(filepath string) (*Cfg, error) {
	if filepath == "" {
		// return no client for velos here and no error
		return nil, nil
	}
	var cfg Cfg
	if content, err := ioutil.ReadFile(filepath); err != nil {
		return nil, fmt.Errorf("Read file error: %s\n", err)
	
	} else if err := json.Unmarshal(content, &cfg); err != nil {
		return nil, fmt.Errorf("JSON error in file: %s %v", filepath, err)
	
	}
	if err := cfg.Normalize(); err != nil {
		return nil, err
	}
	return &cfg, nil
}

func (c *Cfg) Info() string {
	return fmt.Sprintf(`{"addr":%q,"user":%q,"log":%d}`,
		c.Addr, c.User, c.Log,
	)
}

func (c *Cfg) Normalize() error {
	if c.User == "" {
		return fmt.Errorf("Velos server config: Invalid user field")
	}
	if c.Pass == "" {
		return fmt.Errorf("Velos server config: Invalid pass field")
	}
	return nil
}

func (c *Cfg) Form(form *tview.Form) {

	if c == nil {
		return 
	}
	form.AddInputField("Address", c.Addr, 40, nil, func(addr string) {
		c.Addr = addr
	}).
	AddInputField("Username", c.User, 30, nil, func(user string) {
		c.User = user
	}).
	AddPasswordField("Password", c.Pass, 30, '*', func(pass string) {
		c.Pass = pass
	})

	term.NewDropDown("Log",
		term.DropItemInt(0),
		term.DropItemInt(1),
		term.DropItemInt(2),
	).AddForm(form, term.DropItemInt(c.Log), func(item term.DropItem) {
		c.Log = int(item.(term.DropItemInt))
	})
}






func CfgHelp(h *term.Help) {

	h.Print2W("Velos server")

	h.Print4BG("Enable",
		`Click to enable Velos server (simulator).`)

	h.Print4YG("Username",
		`Type the username to be matched by clients.`)

	h.Print4YG("Password",
		`Type the password to be matched by clients.`)

	h.Print4YG("Log",
		`Select the level for logging:
       0 - None.
       1 - Console.`)

	h.Print4BG("Disable",
		`Click to disable Velos server.`)
}


