package  velos

import (
	"context"
	"fmt"

	"github.com/rivo/tview"

	"gitlab.com/jmireles/cans-milk/json-rpc"
	"gitlab.com/jmireles/cans-milk/term"
)

type SimClient struct {

	*term.Term

	cfg *Cfg
	sim *Sim
	r   []*tview.TextView
}

func NewSimClient(ctx context.Context, cfg *Cfg) error {
	if cfg == nil {
		cfg = CfgDefault()
	}
	v := &SimClient{
		Term: term.NewTerm(),
		cfg:  cfg,
		r:    make([]*tview.TextView, 4),
	}
	v.sim = NewSim(cfg, v.log)
	for i := range v.r {
		v.r[i] = tview.NewTextView().
			SetDynamicColors(true)
	}
	header := tview.NewTextView().
		SetTextAlign(tview.AlignCenter).
		SetDynamicColors(true)
	fmt.Fprintf(header, "[white]Velos client simulator")

	grid := tview.NewGrid().
		SetRows(1, 0, 1,1,1,1).
		SetColumns(25, 0).
		AddItem(header,       0, 0, 1,  2, 0, 0, false).
		AddItem(v.controls(), 1, 0, 1,  1, 0, 0, false).
		AddItem(v.Center(),   1, 1, 1,  1, 0, 0, false).
		AddItem(v.r[0],       2, 0, 1,  2, 0, 0, false).
		AddItem(v.r[1],       3, 0, 2,  2, 0, 0, false).
		AddItem(v.r[2],       4, 0, 1,  2, 0, 0, false).
		AddItem(v.r[3],       5, 0, 1,  2, 0, 0, false)

	return v.RunGrid(grid)
}

func (v *SimClient) controls() *tview.Flex {
	list := tview.NewList().
		AddItem("Config",       "", 'v', v.velos).
		AddItem("Measurements", "", 'm', v.measurements).
		AddItem("Help",         "", 'h', func(){ v.help() })

	v.velos()
	return tview.NewFlex().
		SetDirection(tview.FlexRow).
		AddItem(list, 0, 1, false).
		AddItem(tview.NewForm().
			AddButton("Quit", v.Quit), 0, 1, false)
}

func (v *SimClient) velos() {
	v.Form("Config", v.cfg.Form)
}

func (v *SimClient) measurements() {
	v.Form("Measurements", func(form *tview.Form) {
		v.sim.Form(form, v.Center(), v.measurements)
	})
}

func (v *SimClient) log(call *rpc.Call) {
	if call == nil {
		return
	}
	for i := range v.r {
		v.r[i].SetText("")
	}
	call.Rows(v.r[0], v.r[1], v.r[2], v.r[3])
}

func (v *SimClient) help() {
	help := term.NewHelp()
	simClientCfgHelp(help)
	simHelp(help)
	v.CenterSet(help, " Help ")
}

func simClientCfgHelp(h *term.Help) {
	h.Print2W("Config")

	h.Print4YG("URL",
		`Type the Velos server URL including scheme, IP and port.`)

	h.Print4YG("Username",
		`Type the username expected by velos server.`)

	h.Print4YG("Password",
		`Type the password expected by velos server.`)
}

type Sim struct {
	cfg      *Cfg
	log      rpc.Log
	client   *Client
	action    string

	place    int
	reattach bool
	reason   TakeoffReason
}

func NewSim(cfg *Cfg, log rpc.Log) *Sim {
	return &Sim{
		cfg:   cfg,
		log:   log,
		action: "Connect",
	}
}

func (s *Sim) Form(form *tview.Form, flex *tview.Flex, done func()) {
	if s == nil {
		return
	}
	var options = []string { "Ok", "Cancel" }
	if s.client == nil {
		form.AddFormItem(term.NewButtonItem("Client", s.action).
			SetEditor(flex, func(action string) {
				switch action {
				case "Ok":
					s.action = "Disconnect"
					s.client = NewClient(s.cfg, s.log)
				}
				done()
			}).
			SetListSelector(" Connect client? ", options))
		return
	}

	form.AddFormItem(term.NewButtonItem("Client", s.action).
		SetEditor(flex, func(action string) {
			switch action {
			case "Ok":
				s.action = "Connect"
				s.client = nil
			}
			done()
		}).
		SetListSelector(" Disconnect client? ", options))

	form.AddFormItem(term.NewButtonItem("", "Login").
		ButtonClick(func() {
			s.log(s.client.Login())
			done()
		}))

	form.AddFormItem(term.NewButtonItem("", "Logout").
		ButtonClick(func() {
			s.log(s.client.Logout())
			done()
		}))

	term.NewDropDown("Place",
		term.DropItemInt(1),
		term.DropItemInt(2),
		term.DropItemInt(3),
		term.DropItemInt(4),
		term.DropItemInt(5),
	).AddForm(form, term.DropItemInt(s.place), func(item term.DropItem) {
		s.place = int(item.(term.DropItemInt))
	})

	form.AddFormItem(tview.NewCheckbox().
		SetLabel("Start Reattach").
		SetCheckedString(" ✔ ").
		SetChecked(s.reattach).
		SetChangedFunc(func(checked bool) {
			s.reattach = checked
		}))

	term.NewDropDown("Stop reason",
		term.DropItemString(string(Unknown)),
		term.DropItemString(string(Automatic)),
		term.DropItemString(string(Manual)),
		term.DropItemString(string(Reaching)),
	).AddForm(form, term.DropItemString(string(s.reason)), func(item term.DropItem) {
		s.reason = TakeoffReason(item.(term.DropItemString))
	})

	form.
	AddButton("Start", func() {
		s.log(s.client.StartMeasuring(&Start{
			place:    s.place,
			reattach: s.reattach,
		}))
	}).
	AddButton("Stop", func() {
		s.log(s.client.StopMeasuring(&Stop{
			place:  s.place,
			reason: s.reason,
		}))
	})
}

func simHelp(h *term.Help) {
	h.Print2W("Measurements")
	
	h.Print4YG("Client", 
		`Click Connect or Disconnect button to permit or suspend
      this client sending measurements to a velos server.`)
    
    h.Print4YG("Place", 
    	`Select the place or stall where to send starts or stops
      measurements.`)
    
    h.Print4YG("Start Reattach", 
    	`Select the reattach flag to be included with following 
      starts measurements.`)
    
    h.Print4YG("Stop Reason",
    	`Select the reason be included with following stops
      measurements.`)
    
    h.Print4BG("Start", 
    	`Click to send a start measurement. Expect request and response 
      displayed in the footer of the console page.`)
    
    h.Print4BG("Stop",
    	`Click to send a stop measurement. Expect request and response 
      displayed in the footer of the console page.`)
}


