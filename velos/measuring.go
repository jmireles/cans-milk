package velos

import (
	"io"
)

type Start struct {
	place    int
	reattach bool
}

type Stop struct {
	place  int
	reason TakeoffReason
}

type TakeoffReason string

const (
	Unknown   TakeoffReason = "UNKNOWN"
	Automatic TakeoffReason = "AUTOMATIC"
	Manual    TakeoffReason = "MANUAL"
	Reaching  TakeoffReason = "REACHING_ROTOR_POSITION"
)

type Measuring struct {
	start   chan *Start
	stop    chan *Stop
	name    string
	metrics *Metrics
}

func (m *Measuring) Name() string {
	return m.name
}

func (m *Measuring) Metrics(w io.Writer) {
	m.metrics.Write(w)
}

func (m *Measuring) Start(place int, reattach bool) {
	m.start<- &Start{
		place:    place,
		reattach: reattach,
	}
}

func (m *Measuring) Stop(place int, reason TakeoffReason) {
	m.stop<- &Stop{
		place:  place, 
		reason: reason,
	}
}


