package p19

import (
	"context"
	"fmt"
	"sync"
	"time"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk/line"

	"gitlab.com/jmireles/cans-milk/rec"
)

type Apps struct {
	Tcp  *Cfg        `json:"tcp,omitempty"`
	Srvs *CfgSrvs    `json:"services,omitempty"`
	Recs *rec.Cfg   `json:"recs,omitempty"`
	//Mqtt pub.MqttCfg `json:"mqtt,omitempty"`

	wg sync.WaitGroup
}

func AppsDefault() Apps {
	return Apps {
		Tcp:  CfgDefault(),
		Srvs: CfgSrvsDefault(),
		Recs: rec.CfgDefault(),
	}
}

func (c *Apps) Validate() error {
	if tcp := c.Tcp; tcp != nil {
		tcp.Normalize()
	}
	if err := c.Srvs.Validate(); err != nil {
		return fmt.Errorf("json services.%v", err)
	}
	if recs := c.Recs; recs != nil {
		recs.Normalize()
	}
	return nil
}

// Process starts
func (a *Apps) Start(ctx context.Context, w *rec.Writer, msgs *line.Msgs) error {

	p19s, err := NewTcp(ctx, a.Tcp)
	if err != nil {
		return err
	}
	/*var mqtt *line.Mqtt
	if a.Mqtt.Port > 0 {
		mqtt, err = line.NewMqtt(ctx, a.Mqtt)
		if err != nil {
			return err
		}
	}*/

	a.wg.Add(1)
	go func() {
		defer a.wg.Done()
		ctx, cancel := context.WithCancel(ctx)
		defer cancel()

		ticker := time.NewTicker(a.Recs.Save.Duration)
		defer ticker.Stop()
		for {
			select {

			case <- ticker.C:
				w.Flush()

			case resp := <- msgs.Resp():
				p19s.Resp(resp)
				w.AppendMsg(resp)
				//if mqtt != nil {
				//	mqtt.Msg(resp)
				//}

			case req := <- p19s.Req:
				if req.Heartbeat {
					w.IncrHB()
				}
				if can := req.Can; can != nil {
					if now, err := base.UTCNow(); err != nil {
						// silent error?
					} else if req, err := line.NewMsg(can.Net, can.Data, now); err != nil {
						// silent error?
					} else {
						w.AppendMsg(req)
						go msgs.Req(req)
					}
				}

			case <-ctx.Done():
				return
			}
		}
	}()
	return nil
}

func (a *Apps) Services(ctx context.Context) {
	a.wg.Add(1)
	go func() {
		defer a.wg.Done()
		services := NewServices(a.Srvs)
		services.Start(ctx)
	}()
}

func (a *Apps) Wait() {
	a.wg.Wait()
	time.Sleep(1 * time.Second)
}

