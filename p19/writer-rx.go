package p19

import (
	"context"
	"time"
)

type Writer struct {
	c chan []*Client
	f chan []byte
}

func NewWriter(ctx context.Context, delay time.Duration) *Writer {
	
	c := make(chan []*Client, 1)
	f := make(chan []byte, 1)
	
	go func() {

		clients := make([]*Client, 0)
		frames  := make([][]byte, 0)

		ctx, cancel := context.WithCancel(ctx)
		defer cancel()
		ticker := time.NewTicker(delay)
   		defer ticker.Stop()
   		for {
   			select {
   			case c := <-c:
   				// update list of clients, when added or removed
				clients = c

   			case f := <-f:
   				// append new frame, CAN, Heartbeat...
   				frames = append(frames, f)

    		case <- ticker.C:
    			if len(frames) != 0 {
    				// write all frames so far and clean buffer
					if b, err := FrameBytes(frames); b != nil && err == nil {
						for _, c := range clients {
							if c != nil {
								c.write(b)
							}
						}
					}
					frames = frames[:0]
				}

    		case <-ctx.Done():
    			return
    		}
    	}
	}()
	return &Writer{
		c: c, // clients
		f: f, // frames
	}
}

func (w *Writer) Clients(clients map[*Client]bool) {
	keys := make([]*Client, len(clients))
	i := 0
	for client := range clients {
		keys[i] = client
		i++
	}
	w.c<- keys
}

func (w *Writer) AppendCan(resp *Can) {
	w.f<- rxWriteCan(resp)
}

func (w *Writer) AppendHeartbeat() {
	w.f<- []byte{
		rxHearbeat4,
	}
}

func (w *Writer) AppendGetStatus() {
	for _, b := range rxStatusResp() {
		w.f<- b
	}
}


func writerRespStatus() []byte {
	status, _ := FrameBytes(rxStatusResp())
	return status
}

const (
	rxStatus1 byte = 0x01
	rxCan3         = 0x03
	rxHearbeat4    = 0x04
)

// last boumatic-control 2020 version = 10
// use > 0x80 for simulators
// 0x80 since 2021/03/10
const serverVersion = byte(0x80)

// +----------------+---------------+----------+---------------------------------------------+
// |  byte position | field         | value(s) | description                                 |
// +----------------+---------------+----------+---------------------------------------------+
// |              0 | type          | 1        | Command identifying status.                 |
// +----------------+---------------+----------+---------------------------------------------+
// |              1 | version       | 0-FF     | Box firmware version                        |
// |              2 | overrun       | 0-FF     | memory losses by overrun                    |
// |            3-4 | messages      | 0-FFFF   | pending messages to report.                 |
// |      5,6,7,8,9 | can-1-debug-x | 0-FF     | Debug bytes for Can channel 1 (x=1,2,3,4,5) |
// | 10,11,12,13,14 | can-2-debug-x | 0-FF     | Debug bytes for Can channel 2 (x=1,2,3,4,5) |
// | 15,16,17,18,19 | can-3-debug-x | 0-FF     | Debug bytes for Can channel 3 (x=1,2,3,4,5) |
// | 20,21,22,23,24 | can-4-debug-x | 0-FF     | Debug bytes for Can channel 4 (x=1,2,3,4,5) |
// |    25-26-27-28 | memory        | 0-FF     | Memory used.                                |
// +----------------+---------------+----------+---------------------------------------------+
func rxStatusResp() [][]byte {
	frames := [][]byte{
		[]byte{ // Interface Firmware request
			rxStatus1,     // pos=0 always 1
			serverVersion, // pos=1 version 80->simulator
			0x00,          // pos=2 overrun
			0x00, 0x02,    // pos=3,4 messages (L-H)
			0x01, 0x02, 0x03, 0x04, 0x05, // pos= 5- 9 can 1 left --- right
			0x00, 0x00, 0x00, 0x00, 0x00, // pos=10-14 can 2
			0x00, 0x00, 0x00, 0x00, 0x00, // pos=15-19 can 3
			0x00, 0x00, 0x00, 0x00, 0x00, // pos=20-24 can 4
			0x00, 0x01, 0x00, 0x00, // pos=25-28       (L-x-x-H)
		},
	}
	return frames
}







//type Rx interface {
//	RxCan(resp *Can)
//	RxHeartbeat()
//}

/*func rxCanResp(f []byte) *Can {
	if len(f) < 10 {
		return nil
	}
	// ┌─────────┬──────┬────────────┐
	// |       0 | type | 3          |
	// |       1 | net  | 1,2,3,4    |
	// | 2-3-4-5 | time | 0-FFFFFFFF |
	// |       6 | LC   | 0 - FF     |
	// |       7 | D    | 0 - FF     |
	// |       8 | S    | 0 - FF     |
	// |       9 | C    | 0 - FF     |
	// |   10,17 | data | 0 - FF     |
	// └─────────┴──────┴────────────┘
	//      6         7         8      9    10   11   12   13   14   15   16   17
	// ┌─────────┬─────────┬─────────┬────┬────┬────┬────┬────┬────┬────┬────┬────┐
	// │LLLL 0000     0         S     comm                                        │
	// └─────────┴─────────┴─────────┴────┴────┴────┴────┴────┴────┴────┴────┴────┘
	//
	//      6         7         8       9     10        11        12        13         14        15
	// ┌─────────┬─────────┬─────────┬────┬─────────┬─────────┬─────────┬─────────┬─────────┬──────────┐
	// │YYYM-MMMD DDDD-SSSS CCCC-CCCC  31  0000-0000 0000-LLLL YYYM-MMMD DDDD-SSSS CCCC-CCCC IIII-IIIII│
	// └─────────┴─────────┴─────────┴────┴─────────┴─────────┴─────────┴─────────┴─────────┴──────────┘
	if data, err := milk.NewData(f[6:]); err != nil {
		return nil
	} else {
		return &Can{
			Net:  f[1],
			Msec: int64(f[2]) + int64(f[3])<<8 + int64(f[4])<<16 + int64(f[5])<<24,
			Data: data,
		}
	}
}*/


// +---------------+-----------+------------+----------------------+
// | byte position | field     | value(s)   | description          |
// +---------------+-----------+------------+----------------------+
// |             0 | type      | 3          | Ethernet Can message |
// +---------------+-----------+------------+----------------------+
// |             1 | net       | 1,2,3,4    | can network          |
// |       2-3-4-5 | eventTime | 0-FFFFFFFF | Occurrence time      |
// |             6 | LC        | 0 - FF     | messages lines       |
// |             7 | D         | 0 - FF     | destination id       |
// |             8 | S         | 0 - FF     | source id            |
// |             9 | C         | 0 - FF     | command              |
// |         10,17 | data      | 0 - FF     | (optional)           |
// +---------------+-----------+------------+----------------------+
func rxWriteCan(c *Can) []byte {
	data := c.Data.Bytes()
	frame := make([]byte, 6 + len(data))
	frame[0] = rxCan3
	frame[1] = c.Net
	frame[2] = byte(c.Msec)
	frame[3] = byte(c.Msec >> 8)
	frame[4] = byte(c.Msec >> 16)
	frame[5] = byte(c.Msec >> 24)
	copy(frame[6:], data)
	return frame
}


