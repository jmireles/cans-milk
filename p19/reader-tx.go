package p19

import (
	"fmt"
)

type Reader struct {
	parser  *FrameReqs
	state   State
	size    int
	partial []byte
}

func NewReader(parser *FrameReqs) *Reader {
	return &Reader{
		parser: parser,
	}
}

//    0    1   2   3   4            N
// ┌──────┬───┬───┬───┬───┬──   ──┬───┐
// │ 0xAA │  size │   p a y l o a d   │
// │      │       │          ...      │
// └──────┴───┴───┴───┴───┴──   ──┴───┘
func (r *Reader) Read(m []byte) {
	r.stateIn(m)
}

type State int

const (
	StateIn State = iota // default
	StateS1
	StateS2
	StateFrame
)

func (r *Reader) stateIn(in []byte) {
	if len(in) <= 0 {
		return
	}
	switch r.state {
	case StateIn:
		if in[0] == 0xAA {
			// remove 0xAA go for sizes
			r.state = StateS1
			r.stateS1(in[1:])
		} else {
			// remove dirt and recurse (above)
			r.stateIn(in[1:])
		}
	case StateS1:
		r.stateS1(in)
	case StateS2:
		r.stateS2(in)
	case StateFrame:
		r.stateFrame(in)
	}
}

func (r *Reader) stateS1(in []byte) {
	switch len(in) {
	
	case 0:
		return // do nothing, keep stateS1
	
	case 1: // save first state
		r.size = int(in[0])
		r.state = StateS2
		return
	
	default: // 2 or greater, remove 2-sizes a go for payload
		r.size = int(in[0])
		r.size += int(in[1]) << 8
		r.partial = nil
		r.state = StateFrame
		r.stateFrame(in[2:])
	}
}

func (r *Reader) stateS2(in []byte) {
	switch len(in) {
	
	case 0:
		return // keep state_2
	
	default:
		r.size += int(in[0]) << 8
		r.partial = nil
		r.state = StateFrame
		r.stateFrame(in[1:])
	}
}

type Fit int

const (
	FitOk0 Fit = iota
	FitOkMore1
	FitNo2
	FitOkPartial3
	FitOkPartialMore4
	FitNoPartial5
)

func (r *Reader) stateFrame(in []byte) {
	if len(in) == 0 {
		return
	}
	if len(r.partial) == 0 {
		more := len(in) - r.size
		if more == 0 { // FitOk0
			// |<---- size ---->|
			// ┌────────────────┐
			// │       in       │
			// ┌────────────────┐
			// │       ok       │
			// └────────────────┘
			r.frame(FitOk0, in)
			r.state = StateIn
		} else if more > 0 { // FitOkMore1
			// parse complete and go for more
			// |<---- size ---->|
			// ┌───────────────────────────┐
			// │            in             │
			// ┌────────────────┐──────────┘
			// │      ok        │
			// └────────────────┌──────────┐
			//                  │   next   │
			//                  └──────────┘
			ok, next := in[:r.size], in[r.size:]
			r.frame(FitOkMore1, ok)
			r.state = StateIn
			r.stateIn(next)
		} else { // FitNo2
			// store partial and stop
			// |<---- size ---->|
			// ┌─────────┐
			// │   in    │
			// ┌─────────┐
			// │ partial │
			// └─────────┘
			r.partial = make([]byte, len(in))
			copy(r.partial, in)
			r.frame(FitNo2, nil)
		}
	} else {
		rest := r.size - len(r.partial)
		more := len(in) - rest
		if more == 0 { // FitOkPartial3
			// |<---- size -------->|
			// ┌─────────┐
			// │ partial │<- rest ->| more == 0
			// └─────────┌──────────┐
			//           │    in    │
			// ┌────────────────────┐
			// │        out         │
			// └────────────────────┘
			out := append(r.partial, in...)
			r.frame(FitOkPartial3, out)
			r.partial = nil
			r.state = StateIn
		} else if more > 0 { // FitOkPartialMore4
			// |<----- size ------->|
			// ┌─────────┐
			// │ partial │<- rest ->|<- more ->|
			// └─────────┌─────────────────────┐
			//           │         in          │
			// ┌────────────────────┐──────────┘
			// │        out         │
			// └────────────────────┌──────────┐
			//                      │   next   │
			//                      └──────────┘
			out := append(r.partial, in[:rest]...)
			next := in[rest:]
			r.frame(FitOkPartialMore4, out)
			r.partial = nil
			r.state = StateIn
			r.stateIn(next)
		} else { // FitNoPartial5
			// |<---- size -------->|
			// ┌─────────┐
			// │ partial │<- rest ->|
			// └─────────┌────┐
			//           │ in │
			// ┌──────────────┐
			// │ new-partial  │<--->| new rest
			// └──────────────┘
			r.partial = append(r.partial, in...)
			r.frame(FitNoPartial5, nil)
			// exit keep current state
		}
	}
}

var (
	ErrorEmpty        = fmt.Errorf("A5 empty")
	ErrorA5Missing    = fmt.Errorf("A5 missing")
	ErrorA5Mismatch   = fmt.Errorf("A5 mismatch")
	ErrorSizeMissing  = fmt.Errorf("A5 size missing")
	ErrorA5Incomplete = fmt.Errorf("A5 incomplete")
)

//   0   1   2   3   4      5     6    7
// ┌───┬───┬───┬───┬───┬──────┬──────┬───┬─       ─┬───┐
// │ milliseconds  │ n │ 0xA5 │ size │      Frame      │
// └───┴───┴───┴───┴───┴──────┴──────┴───┴─       ─┴───┘
//                     │ 0xA5 │ size │      Frame      │
//                     └──────┴──────┴───┴─       ─┴───┘
//                      ...
//                     ┌──────┬──────┬───┬─       ─┬───┐
//                     │ 0xA5 │ size │      Frame      │
//                     └──────┴──────┴───┴─       ─┴───┘
func (r *Reader) frame(fit Fit, b []byte) {
	if len(b) == 0 {
		r.frames(fit, 0, nil, nil)
		return
	}
	if len(b) < 5 {
		r.frames(fit, 0, nil, ErrorEmpty)
		return
	}
	time := int64(b[0]) + int64(b[1])<<8 + int64(b[2])<<16 + int64(b[3])<<24
	n := int(b[4])
	frames := make([]Frame, n)
	pos := 5
	for i := 0; i < n; i++ {
		if pos >= len(b) {
			r.frames(fit, time, frames, ErrorA5Missing)
			return
		}
		if 0xA5 != b[pos] {
			r.frames(fit, time, frames, ErrorA5Mismatch)
			return
		}
		pos++ // 6
		if pos >= len(b) {
			r.frames(fit, time, frames, ErrorSizeMissing)
			return
		}
		size := int(b[pos])
		pos++
		frame := make([]byte, size)
		copy(frame, b[pos:pos+size])
		frames[i] = frame
		pos += size
	}
	r.frames(fit, time, frames, nil)
}

func (r *Reader) frames(fit Fit, time int64, frames []Frame, err error) {
	r.parser.fireFrames(&Frames{
		Fit:    fit, 
		Time:   time, 
		Frames: frames, 
		Err:    err,
	})
	for _, frame := range frames {
		if len(frame) > 0 {
			command := frame[0]
			// cansServer respond commands
			// to be parsed by cansClient
			txFrame(r.parser, command, frame)
		}
	}
}


// com.boumatic.protocol.ethernet.TxReq
// to be parsed by the cans.Server
const (
	txGetStatus1 byte = 1
	txGetConfig2      = 2
	txSetConfig3      = 3
	txGetFirstData4   = 4
	txSetCan5         = 5
	txGetNextData6    = 6
	txSetHeartBeat7   = 7
)

type Tx interface {
	TxGetStatus()
	TxGetConfig()
	TxSetConfig(f []byte)
	TxGetFirstData(f []byte)
	TxSetCan(req *Can)
	TxGetNextData()
	TxSetHeartbeat()
}

func txFrame(reqs *FrameReqs, comm byte, frame []byte) {
	switch comm {

	case txGetStatus1:
		reqs.fireStatus()	
	
	case txSetCan5:
		if req := txCan(frame); req != nil {
			reqs.fireCan(req)
		}
	
	case txSetHeartBeat7:
		reqs.fireHeartbeat()
	}
}

func txReq(tx Tx, command byte, f []byte) {
	switch command {
	
	case txGetStatus1:
		tx.TxGetStatus()
	
	case txGetConfig2:
		tx.TxGetConfig()
	
	case txSetConfig3:
		tx.TxSetConfig(f)
	
	case txGetFirstData4:
		tx.TxGetFirstData(f)
	
	case txSetCan5:
		if req := txCan(f); req != nil {
			tx.TxSetCan(req)
		}
	
	case txGetNextData6:
		tx.TxGetNextData()
	
	case txSetHeartBeat7:
		tx.TxSetHeartbeat()
	}
}

func txCan(f []byte) *Can {
	//  0 1         6
	// |C|N|t|t|t|t|d|d|d|d|d|d|d|d|d|d
	net := f[1] // 1,2,3,4
	data := f[2:]
	return NewCan(net, data)
}



