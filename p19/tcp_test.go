package p19

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"testing"
	"time"
)

func TestTCP(t *testing.T) {

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	// before run node+java
	// cd /rt2019
	// sudo pm2 start node/parlor-services.js
	// sudo pm2 start parlor-jar.sh
	// Go to localhost:8080 and send requests
	t.Run("TCP server", func( t *testing.T) {
		cfg := CfgDefault()
		cfg.Log = 3
		tcp, err := NewTcp(ctx, cfg)
		if err != nil {
			t.Fatal(err)
		}
		ctx, cancel := context.WithCancel(ctx)
		defer cancel()
		for {
			select {
			case req := <-tcp.Req:
				// requests from p19 TCP client
				if req.Heartbeat {
					fmt.Println("TCP heartbeat")
				}
				if can := req.Can; can != nil {
					if req := tcp.Request(can); req != nil {
						// send request to CANs equipment from Java client
						fmt.Println("TCP request", req)
					}
				}
			case <-ctx.Done():
				return
			}
		}
		time.Sleep(10 * time.Second)
	})
}

