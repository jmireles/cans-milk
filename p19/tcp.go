package p19

import (
	"context"
	"fmt"
	"net"
	"time"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk/line"
)

type Tcp struct {
	writer *Writer
	Req    <-chan *FrameReq
	cfg    *Cfg
}

func NewTcp(ctx context.Context, cfg *Cfg) (*Tcp, error) {
	reqs := NewFrameReqs()
	writer := NewWriter(ctx, cfg.Frame.Duration)
	reader := NewReader(reqs)
	if err := tcpClients(writer, reader, cfg); err != nil {
		return nil, err
	}
	cfg.Logger(1, "Start port=%d Heartbeat=%s", cfg.Port, cfg.Heartbeat)
	return &Tcp{
		writer: writer,
		Req:    reqs.Req(),
		cfg:    cfg,
	}, nil
}

func (t *Tcp) Request(can *Can) *line.Msg {
	if now, err := base.UTCNow(); err != nil {
		t.cfg.Logger(2, "Request time error %v", err)						
		return nil
	} else if req, err := line.NewMsg(can.Net, can.Data, now); err != nil {
		t.cfg.Logger(2, "Request line error %v", err)
		return nil
	} else {
		t.cfg.Logger(2, "Request %v", req)
		return req
	}
}

func (t *Tcp) Resp(msg *line.Msg) {
	can := NewCanMsg(msg)
	t.writer.AppendCan(can)
	t.cfg.Logger(3, "Response:%v", can)
}

func tcpClients(writer *Writer, reader *Reader, cfg *Cfg) error {
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", cfg.Port))
	if err != nil {
		return err
	}
	add    := make(chan *Client)
	remove := make(chan *Client)
	go func() {
		clients := make(map[*Client]bool)
		for {
			select {
			
			case client := <-add:
				clients[client] = true
				writer.Clients(clients)
				cfg.Logger(1, "Client added, clients=%d", len(clients))
			
			case client := <-remove:
				if _, ok := clients[client]; ok {
					close(client.data)
					delete(clients, client)
				}
				writer.Clients(clients)
				cfg.Logger(1, "Client removed, clients=%d", len(clients))
			}
		}
	}()
	go func() {
		for {
			conn, err := listener.Accept() // conn
			if err != nil {
				fmt.Println(err)
				return
			}
			client := newClient()
			add <- client
			
			go func() {
				// receiver
				a := make([]byte, 4096)
				for {
					if length, err := conn.Read(a); err != nil {
						remove <- client
						conn.Close()
						break
					} else if length > 0 {
						m := a[0:length]
						reader.Read(m)
					}
				}
			}()
			
			go func() {
				// sender
				ticker := time.NewTicker(cfg.Heartbeat.Duration)
				defer conn.Close()
				for {
					select {
					case mess, ok := <-client.data:
						if !ok {
							return
						}
						conn.Write(mess)
					
					case <-ticker.C:
						writer.AppendHeartbeat()
					}
				}
			}()
		}
	}()
	return nil
}


type Client struct {
	data chan []byte
}

func newClient() *Client {
	client := &Client{
		data: make(chan []byte, 1),
	}
	return client
}

func (c *Client) write(bytes []byte) {
	defer func() {
		// prevent write on closed channel
		if r := recover(); r != nil {
			return
		}
	}()
	c.data<- bytes
}



