package p19

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"golang.org/x/net/html"
	"golang.org/x/net/html/charset"
	"golang.org/x/text/encoding"
	"golang.org/x/text/transform"

	"gitlab.com/jmireles/cans-base"
)

type Services struct {
	cfg *CfgSrvs
	log base.Log
}

func NewServices(cfg *CfgSrvs) *Services {
	var log base.Log
	if cfg.Log > 0 {
		log = func(f string, args ...any) {
			fmt.Print("Srvs: ")
			fmt.Printf(f, args...)
			fmt.Println()
		}
	}
	return &Services{
		cfg: cfg,
		log: log,
	}
}

func (s *Services) Start(ctx context.Context) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// check apps running by calling GET, try to start them.
	s.health(ctx)

	ticker := time.NewTicker(s.cfg.Health.Duration)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			// Check apps still are running. Try restart them.
			s.health(ctx)

		case <-ctx.Done():
			return
		}
	}
}

func (s *Services) health(ctx context.Context) {
	html, err := s.httpGet()
	if err == nil {
		if s.log != nil {
			s.log("%sHealth ok: %v%s", base.Blue, html, base.Reset)
		}
		return
	}
	if s.log != nil {
		s.log("%sHealth error: %v%s", base.Red, err, base.Reset)
	}
	
	go func() {
		if s.log != nil {
			s.log("%sTry startup with restart:%v%s", base.Blue, s.cfg.Restart, base.Reset)
		}
		// Run the apps and destroy after timeout to simulate failed
		// Next ServicesCheck invocation with a Ticker would Run again
		ctx, cancel := context.WithTimeout(ctx, s.cfg.Restart.Duration)
		defer cancel()
		if err := s.run(ctx); err != nil {
			if s.log != nil {
				s.log("%sRun error: %v%s", base.Red, err, base.Reset)
			}
		}
		<-ctx.Done()
		if s.log != nil {
			s.log("%sStop error: %v%s", base.Yellow, ctx.Err(), base.Reset)
		}
	}()
}

func (s *Services) run(ctx context.Context) error {
	path := ""
	if s.cfg.Home {
		if home, err := os.UserHomeDir(); err != nil {
			return err
		} else {
			path = filepath.Join(home, s.cfg.Folder)
		}
	} else {
		path = s.cfg.Folder
	}
	rt := NewSrv(path)
	time.Sleep(1 * time.Second)
	var nodeLogF base.Log
	var javaLogF base.Log
	if s.cfg.Log > 1 {
		nodeLogF = func(f string, args ...any) {
			fmt.Print("Node: ")
			fmt.Printf(f, args...)
			fmt.Println()
		}
		javaLogF = func(f string, args ...any) {
			fmt.Print("Java: ")
			fmt.Printf(f, args...)
			fmt.Println()
		}
	}
	if err := rt.Node(ctx, nodeLogF); err != nil {
		return err
	}
	time.Sleep(1 * time.Second)
	if err := rt.Java(ctx, javaLogF); err != nil {
		return err
	}
	return nil
}

func (s *Services) httpGet() (string, error) {
	now := time.Now()
	req, err := http.NewRequest("GET", s.cfg.URL, nil)
	if err != nil {
		return "", err
	}
	req.Header.Set("Cache-Control", "no-cache")
	client := &http.Client{
		Timeout: s.cfg.Client.Duration,
	}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	ms := time.Now().Sub(now).Milliseconds()
	if serial, version, err := s.httpResp(body); err != nil {
		return "", err
	} else {
		const k = "serial=%s version=%s bytes=%d ms=%d"
		return fmt.Sprintf(k, serial, version, len(body), ms), nil
	}
}

func (s *Services) httpResp(b []byte) (serial, version string, parseErr error) {
	e, _, _, err := s.toUTF8(bytes.NewReader(b)) // name, certain
	if err != nil {
		parseErr = err
		return
	}
	//fmt.Println("name,certain", name, certain)
	r := transform.NewReader(bytes.NewReader(b), e.NewDecoder())

	// Inspect only span where to find Serial/Version
	textTags := []string { "span" }
	tag := ""
	attrs := map[string]string{}
	enter := false
	tokenizer := html.NewTokenizer(r)
	for {
		tt := tokenizer.Next()
		token := tokenizer.Token()
		err := tokenizer.Err()
		if err == io.EOF {
			break
		}
		switch tt {
		case html.ErrorToken:
			parseErr = err
			return
		case html.StartTagToken, html.SelfClosingTagToken:
			enter = false
			attrs = map[string]string{}
			tag = token.Data
			for _, ttt := range textTags {
				if tag == ttt {
					enter = true
					for _, attr := range token.Attr {
						attrs[attr.Key] = attr.Val
					}
					break
				}
			}
		case html.TextToken:
			if enter {
				data := strings.TrimSpace(token.Data)
				if len(data) > 0 {
					switch tag {
					case "span":
						title := strings.ToLower(attrs["title"])
						if (strings.Contains(title, "version")) {
							version = data
						}
						if (strings.Contains(title, "serial")) {
							serial = data
						}
					}
				}
			}
		}
	}
	return
}

func (s *Services) toUTF8(r io.Reader) (e encoding.Encoding, name string, certain bool, err error) {
	b, err := bufio.NewReader(r).Peek(1024)
	if err != nil {
		return
	}
	e, name, certain = charset.DetermineEncoding(b, "")
	return
}






type Srv struct {
	dir string
	wg  sync.WaitGroup
}

func NewSrv(dir string) *Srv {
	return &Srv{
		dir: dir,
	}
}

func (s *Srv) Node(ctx context.Context, log base.Log) error {
	node := exec.CommandContext(ctx, "node",
		`./node/parlor-services.js`,
		`--node-args="--use-openssl-ca"`,
	)
	node.Dir = s.dir
	return s.srv(node, log)
}

func (s *Srv) Java(ctx context.Context, log base.Log) error {
	java := exec.CommandContext(ctx, "java",
		"-jar",
		"ParlorRuntime.jar",
	)
	java.Dir = s.dir
	return s.srv(java, log)
}

func (s *Srv) srv(cmd *exec.Cmd, log base.Log) error {
	// IMPOSrvANT! ParlorRuntime.jar program declares a stdin 
	// as a listener in com.boumatic.data.HttpInfo to help to close the jar.
	// Node spawn child for the same jar does not need to declare the stdin
	// But this go invocation MUST!
	stdin, err := cmd.StdinPipe()
	if err != nil {
		return err
	}
	_  = stdin
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return err
	}
	stderr, err := cmd.StderrPipe()
	if err != nil {
		return err
	}
	go func() {
		s := bufio.NewScanner(stdout)
		for s.Scan() {
			if log != nil {
				log("%s%s%s", base.Green, s.Text(), base.Reset)
			}
		}
	}()
	go func() {
		s := bufio.NewScanner(stderr)
		for s.Scan() {
			if log != nil {
				log("%s%s%s", base.Red, s.Text(), base.Reset)
			}
		}
	}()
	if err := cmd.Start(); err != nil {
		return err
	}
	s.wg.Add(1)
	go func() {
		defer s.wg.Done()
		state, _ := cmd.Process.Wait()
		if log != nil {
			log("%sExit code:%v%s", base.Yellow, state.ExitCode(), base.Reset)
		}
	}()
	return nil
}







