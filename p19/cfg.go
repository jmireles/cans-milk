package p19

import (
	"fmt"
	"time"

	"github.com/rivo/tview"
	"gitlab.com/jmireles/cans-base"

	"gitlab.com/jmireles/cans-milk/term"
)

const (
	cfgDefaultPort  = 2020
	cfgMinFrame     = 100 * time.Millisecond
	cfgMinHeartbeat = 3 * time.Second
)

type Cfg struct {
	Port      int       `json:"port"`
	Frame     base.Time `json:"frame"`
	Heartbeat base.Time `json:"heartbeat`
	Log       int       `json:"log"`
}

func CfgDefault() *Cfg {
	return &Cfg{
		Port:      cfgDefaultPort,
		Frame:     base.Time{ cfgMinFrame },
		Heartbeat: base.Time{ cfgMinHeartbeat },
		Log:       0,
	}
}

func (c *Cfg) Info() string {
	return fmt.Sprintf(`{"port":%d,"frame":%q,"heartbeat":%q,"log":%d}`,
		c.Port, c.Frame, c.Heartbeat, c.Log)
}

func (c *Cfg) Normalize() {
	if c.Port < 2020 || c.Port > 2021 {
		c.Port = 2020
	}
	if min := 100*time.Millisecond; c.Frame.LowerThan(min) {
		c.Frame.Duration = min
	}
	if max := 1*time.Second; c.Frame.GreaterThan(max) {
		c.Frame.Duration = max
	}

	if min := 1 * time.Second; c.Heartbeat.LowerThan(min) {
		c.Heartbeat.Duration = min
	}
	if max := 5 * time.Second; c.Heartbeat.GreaterThan(max) {
		c.Heartbeat.Duration = max
	}
}

func (c *Cfg) Form(form *tview.Form) {

	if c == nil {
		return
	}

	term.NewDropDown("Port",

		term.DropItemInt(cfgDefaultPort),     // 2021
		term.DropItemInt(cfgDefaultPort + 1), // 2022
	
	).AddForm(form, term.DropItemInt(c.Port), func(item term.DropItem) {
		c.Port = int(item.(term.DropItemInt))
	})

	term.NewDropDown("Frame",
		term.DropItemDuration(cfgMinFrame),
	).AddForm(form, term.DropItemDuration(c.Frame.Duration), func(item term.DropItem) {
		c.Frame.Duration = time.Duration(item.(term.DropItemDuration))
	})

	term.NewDropDown("Heartbeat",
		term.DropItemDuration(cfgMinHeartbeat),
	).AddForm(form, term.DropItemDuration(c.Heartbeat.Duration), func(item term.DropItem) {
		c.Heartbeat.Duration = time.Duration(item.(term.DropItemDuration))
	})

	term.NewDropDown("Log",
		term.DropItemInt(0),
		term.DropItemInt(1),
		term.DropItemInt(2),
		term.DropItemInt(3),
	).AddForm(form, term.DropItemInt(c.Log), func(item term.DropItem) {
		c.Log = int(item.(term.DropItemInt))
	})
}

func (c *Cfg) Logger(log int, f string, args ...any) {
	if c.Log >= log {
		fmt.Print("P19: ")
		fmt.Printf(f, args...)
		fmt.Println()
	}
}

func CfgHelp(t *term.Help) {

	t.Print2W("P19 TCP server")

	t.Print4BG("Enable",
		`Enable the TCP server for P19 clients (Java).`)

	t.Print4YG("Port",
		`Select the TCP port. Default is 2020.`)

	t.Print4YG("Frame",
		`The delay to process the TCP frames.`)

	t.Print4YG("Heartbeat",
		`The heartbeat frequency to notify clients.`)

	t.Print4YG("Log",
		`Level of log:
      0 - None.
      1 - Start and Add/Remove TCP Clients.
      2 - Level 1 plus TCP Requests.
      3 - Level 2 plus TCP Responses.`)

	t.Print4BG("Disable",
		`Disable the TCP server.`)
}





type CfgSrvs struct {
	Home    bool      `json:"home"`
	Folder  string    `json:"folder"`
	Restart base.Time `json:"restart"`
	Health  base.Time `json:"health"`
	URL     string    `json:"url"`
	Client  base.Time `json:"client"`
	Log     int       `json:"log"`     // 0=off 1=health 2=helth+node|java
}

func CfgSrvsDefault() *CfgSrvs {
	return &CfgSrvs{
		Home:    true,
		Folder:  "rt2019",
		Restart: base.Time{ 1*time.Minute }, // destroy apps after 1 minute
		Health:  base.Time{ 20*time.Second }, // check every 20s apps are running
		URL:     "http://localhost:8080",
		Client:  base.Time{ 5*time.Second },
		Log:     0,
	}
}

func (s CfgSrvs) Validate() error {
	if s.Folder == "" {
		return fmt.Errorf("folder: empty")
	}
	if s.Restart.LowerThan(1 * time.Minute) {
		return fmt.Errorf("restart: lower than 1 minute")
	}
	if s.Health.LowerThan(20 * time.Second) {
		return fmt.Errorf("health: lower than 20 seconds")
	}
	if s.URL == "" {
		return fmt.Errorf("url: empty")
	}
	if s.Client.LowerThan(5 * time.Second) {
		return fmt.Errorf("client: lower than 5 seconds")
	}
	return nil
}



