package cert

import (
	"fmt"

	"github.com/rivo/tview"

	"gitlab.com/jmireles/cans-milk/term"
)

type Cfg struct {
	Crt string `json:"crt"`
	Key string `json:"key"`
}

func CfgDefault() *Cfg {
	return &Cfg{
		
	}
}

func (c *Cfg) Form(form *tview.Form, flex *tview.Flex, done func()) {

	if c == nil {
		return
	}

	const pub = " Certificates: Select public .crt file "
	const pri = " Certificates: Select private .key file "
	
	form.AddFormItem(term.NewButtonItem("Public .crt", c.Crt).
		SetEditor(flex, func(file string) {
			c.Crt = file
			done()
		}).
		SetFileSelector(pub, []string { ".crt" }))
	
	form.AddFormItem(term.NewButtonItem("Private .key", c.Key).
		SetEditor(flex, func(file string) {
			c.Key = file
			done()
		}).
		SetFileSelector(pri, []string { ".key" }))
}

func (c *Cfg) Info() string {
	return fmt.Sprintf(
		`{"crt":%q,"key":%q}`,
		c.Crt, c.Key,
	)
}

func (c *Cfg) Normalize() {
	// do nothing
}

func CfgHelp(h *term.Help) {

	h.Print2W("Certificates")

    h.Print4BG("Enable",
      `Click to enable certificates. Enable will display two controls 
      to set the public and private certificates files locations.`)

    h.Print4YG("Public .crt",
      `Click and type over button to open a tree panel to locate the 
      public .crt file within the local filesystem.`)

    h.Print4YG("Private .key",
      `Click and type over button to open a tree panel to locate the 
      private .key file within the local filesystem.`)

    h.Print4BG("Disable",
      `Click to disable certificates.`)
}