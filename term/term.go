package term

import (
	"fmt"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

type Term struct {
	app    *tview.Application
	pages  *tview.Pages
	center *tview.Flex
}

func NewTerm() *Term {
	return &Term{
		app:    tview.NewApplication(),
		pages:  tview.NewPages(),
		center: tview.NewFlex().SetDirection(tview.FlexRow),
	}
}

func (t *Term) Draw() {
	t.app.Draw()
}

func (t *Term) RunGrid(grid *tview.Grid) error {
	t.pages.AddPage("run", grid, true, true)
	return t.app.SetRoot(t.pages, true).
		EnableMouse(true).
		Run()
}

func (t *Term) Quit() {
	t.ModalYesNo("Quit?", tcell.ColorOrangeRed, tcell.ColorWhite, func() {
		t.app.Stop()
	})
}

func (t *Term) CenterSet(primitive tview.Primitive, title string) {
	t.center.Clear().
		AddItem(primitive, 0, 1, true).
		SetBorder(true).SetTitle(title)
}

func (t *Term) Center() *tview.Flex {
	return t.center
}

func (t *Term) ModalYesNo(text string, bg, fg tcell.Color, yes func()) {
	id := "modal"
	t.pages.HidePage(id).RemovePage(id)
	modal := tview.NewModal().
		SetText(text).
		SetBackgroundColor(bg).
		SetTextColor(fg).
		AddButtons([]string{ "Yes", "No" }).
		SetDoneFunc(func(btnIndex int, btnLabel string) {
			if btnIndex == 0 {
				yes()
			}
			t.pages.HidePage(id).RemovePage(id)
		})
	t.pages.AddPage(id, modal, false, true) // resize,show
}

func (t *Term) Modal(msg string, err error) {
	id := "modal"
	color := tcell.ColorWhite
	if err != nil {
		color = tcell.ColorRed
		msg = err.Error()
	}
	t.pages.HidePage(id).RemovePage(id)
	t.pages.AddPage(id, tview.NewModal().
			SetBackgroundColor(color).
			SetText(msg).
			AddButtons([]string{ "Ok" }).
			SetDoneFunc(func(btnIndex int, btnLabel string) {
				t.pages.HidePage(id).RemovePage(id)
			}),
		false,
		true,
	)
}

func (t *Term) Form(title string, add func(form *tview.Form)) {
	form := tview.NewForm()
	if add != nil {
		add(form)
	}
	t.CenterSet(form, fmt.Sprintf(" %s ", title))
}

func (t *Term) FormEnabled(title string, disabled func() bool, enable func(bool), refresh func(), add func(form *tview.Form)) {
	form := tview.NewForm()
	if disabled() {
		form.AddButton("Enable", func() {
			enable(true)
			refresh()
		})
		t.CenterSet(form, fmt.Sprintf(" %s (disabled) ", title))
	} else {
		form.AddButton("Disable", func() {
			t.ModalYesNo(fmt.Sprintf("Disable %s?", title), tcell.ColorOrangeRed, tcell.ColorWhite, func() {
				enable(false)
				refresh()
			})
		})
		if add != nil {
			add(form)
		}
		t.CenterSet(form, fmt.Sprintf(" %s (enabled) ", title))
	}
}

type Help struct {
	*tview.TextView
}

func NewHelp() *Help {
	return &Help{
		tview.NewTextView().
			SetWrap(true).
			SetDynamicColors(true),
	}
}

func (h *Help) Print2W(dl string) {
	fmt.Fprintf(h, "\n  [white]%s\n", dl)
}

func (h *Help) Print4YG(tt string, td string) {
	fmt.Fprintf(h, "\n    [yellow]%s\n      [green]%s\n", tt, td)
}

func (h *Help) Print4BG(tt string, td string) {
	fmt.Fprintf(h, "\n    [blue]%s\n      [green]%s\n", tt, td)
}

