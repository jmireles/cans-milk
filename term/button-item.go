package term

import (
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

// ButtonItem uses Checkbox to implement a ButtonItem to be used
// in forms where right side controls is not a checkbox but a button
// which label can be changed for instance after selecting a file from a filesystem.
// See SetChangeFileSelectorFunc
type ButtonItem struct {
	*tview.Checkbox

	editor *tview.Flex
	edited func(string)
}

// Convert a checkbox item to button item for forms
func NewButtonItem(label, value string) *ButtonItem {
	return &ButtonItem{
		Checkbox: tview.NewCheckbox().
			SetLabel(label).
			SetCheckedString(" " + value + " ").
			SetChecked(true),
	}
}

func (b *ButtonItem) SetEditor(editor *tview.Flex, edited func(string)) *ButtonItem {
	b.editor = editor
	b.edited = edited
	return b
}

func (b *ButtonItem) ButtonClick(click func()) *ButtonItem {
	b.Checkbox.SetChangedFunc(func(checked bool) {
		click()
	})
	return b
}

func (b *ButtonItem) SetListSelector(title string, list []string) *ButtonItem {
	b.Checkbox.SetChangedFunc(func(checked bool) {
		// Every time this button is clicked, set this flex with:
		// A cancel button in first row to select "nothing"
		// A tree in second row to select a file filtered from current filesystem
		form := tview.NewForm()
		for _, elem := range list {
			e := elem
			form.AddButton(elem, func() {
				b.edited(e)
			})
		}
		b.editor.Clear().
			SetDirection(tview.FlexRow).
			AddItem(form, 3, 1, true).
			SetBorder(true).SetTitle(title)
	})
	return b
}

func (b *ButtonItem) SetFileSelector(title string, filters []string) *ButtonItem {
	b.Checkbox.SetChangedFunc(func(checked bool) {
		// Every time this button is clicked, set this flex with:
		// A cancel button in first row to select "nothing"
		// A tree in second row to select a file filtered from current filesystem
		b.editor.Clear().
			SetDirection(tview.FlexRow).
			AddItem(tview.NewForm().AddButton("Cancel", func(){
				b.edited("")
			}), 3, 1, true).
			AddItem(b.fileSelectorTree(filters), 0, 1, true).
			SetBorder(true).SetTitle(title)
	})
	return b
}

func (b *ButtonItem) fileSelectorTree(filters []string) *tview.TreeView {
	accept := func(file fs.FileInfo) bool {
		if file.IsDir() {
			return true
		}
		for _, filter := range filters {
			if strings.HasSuffix(file.Name(), filter) {
				return true
			}
		}
		return false
	}
	// A helper function which adds the files and directories of the given path
	// to the given target node.
	add := func(target *tview.TreeNode, path string) {
		files, err := ioutil.ReadDir(path)
		if err != nil {
			panic(err)
		}
		for _, file := range files {
			if accept(file) {
				node := tview.NewTreeNode(file.Name()).
					SetReference(&FileRef{
						path: filepath.Join(path, file.Name()),
						file: file,
					}).
					SetSelectable(true)
				if file.IsDir() {
					node.SetColor(tcell.ColorGreen)
				}
				target.AddChild(node)
			}
		}
	}
	rootDir := "."
	root := tview.NewTreeNode("cwd").SetColor(tcell.ColorRed)
	add(root, rootDir)
	return tview.NewTreeView().
		SetRoot(root).
		SetCurrentNode(root).
		SetDoneFunc(func(k tcell.Key) {
			// Esc,Tab,Back abort
			b.edited("")
		}).
		SetSelectedFunc(func(node *tview.TreeNode) {
			reference := node.GetReference()
			if reference == nil {
				return // Selecting the root node does nothing.
			}
			children := node.GetChildren()
			if len(children) == 0 {
				// Load and show files in this directory.
				ref := reference.(*FileRef)
				if ref.file.IsDir() {
					add(node, ref.path)
				} else {
					// Filename was clicked, selection done
					fullPath := b.joinWorkingDir(ref.path)
					b.edited(fullPath)
				}
			} else {
				// Collapse if visible, expand if collapsed.
				node.SetExpanded(!node.IsExpanded())
			}
		})
}

func (b *ButtonItem) joinWorkingDir(file string) string {
	if file == "" {
		return ""
	}
	wd, _ := os.Getwd()
	return filepath.Join(wd, file)
}

type FileRef struct {
	path string
	file fs.FileInfo
}
