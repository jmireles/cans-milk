package term

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/rivo/tview"
)

type DropItem interface {
	
	// String return the string representation of this Item
	String() string
	
	// Equals return true when comparing against other Item
	Equals(DropItem) bool
}

type DropDown struct {
	label string
	items []DropItem
}

func NewDropDown(label string, items ...DropItem) *DropDown {
	return &DropDown{
		label: label,
		items: items,
	}
}

func (d *DropDown) AddForm(form *tview.Form, item DropItem, update func(DropItem)) *tview.Form {
	return form.AddFormItem(tview.NewDropDown().
		SetLabel(d.label).
		SetOptions(d.options(), func(op string, index int){
			update(d.items[index])
		}).
		SetCurrentOption(d.current(item)))
}

func (d *DropDown) options() []string {
	s := make([]string, len(d.items))
	for p, i := range d.items {
		s[p] = i.String()
	}
	return s
}

func (d *DropDown) current(j DropItem) int {
	for p, i := range d.items {
		if i.Equals(j) {
			return p
		}
	}
	return 0
}


type DropItemInt int

func (i DropItemInt) String() string {
	return fmt.Sprintf(" %d ", i)
}

func (i DropItemInt) Equals(j DropItem) bool {
	return i == j.(DropItemInt)
}

// DropItemString
type DropItemString string

func (d DropItemString) String() string {
	return fmt.Sprintf(" %s ", string(d))
}

func (d DropItemString) Equals(d1 DropItem) bool {
	return d == d1.(DropItemString)
}

// DropItemDuration
type DropItemDuration time.Duration

func (d DropItemDuration) String() string {
	return fmt.Sprintf(" %s ", Time{ time.Duration(d) })
}

func (d DropItemDuration) Equals(d1 DropItem) bool {
	return d == d1.(DropItemDuration)
}





// copied from cans-base to let this file be independent
type Time struct {
	time.Duration
}

func NewTime(d time.Duration) *Time {
	return &Time{
		Duration: d,
	}
}

func (t Time) MarshalJSON() ([]byte, error) {
    return json.Marshal(t.String())
}

func (t *Time) UnmarshalJSON(b []byte) error {
    var v interface{}
    if err := json.Unmarshal(b, &v); err != nil {
        return err
    }
    switch value := v.(type) {
    case string:
        var err error
        t.Duration, err = time.ParseDuration(value)
        if err != nil {
            return err
        }
        return nil
    default:
        return fmt.Errorf("invalid time")
    }
}

func (t *Time) LowerThan(d time.Duration) bool {
	return t.Duration < d
}

func (t *Time) GreaterThan(d time.Duration) bool {
	return t.Duration > d
}


