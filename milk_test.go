package milk

import (
	"testing"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-milk/apps"
	"gitlab.com/jmireles/cans-milk/cert"
	"gitlab.com/jmireles/cans-milk/mqtt"
	"gitlab.com/jmireles/cans-milk/p19"
	"gitlab.com/jmireles/cans-milk/recs"
	"gitlab.com/jmireles/cans-milk/velos"
	"gitlab.com/jmireles/cans-milk/velos/server"
)

func TestCfgs(t *testing.T) {

	tt := base.Test{ T:t }

	file := &apps.GuiFile{
		Cert:     cert.CfgDefault(),
		Mqtt:     mqtt.CfgDefault(),
		P19:      p19.CfgDefault(),
		Recs:     recs.CfgDefault(),
		Velos:    velos.CfgDefault(),
		VelosSrv: server.CfgDefault(),
	}
	tt.Equals(`{"crt":"","key":""}`, file.Cert.Info())
	tt.Equals(`{"port":1883,"user":"","lines":[false,false,false,false],"log":0}`, file.Mqtt.Info())
	tt.Equals(`{"port":2020,"frame":"100ms","heartbeat":"3s","log":0}`, file.P19.Info())
	tt.Equals(`{"save":"5s","log":0}`, file.Recs.Info())
	tt.Equals(`{"url":"","user":"","heartbeat":"5s","log":0}`, file.Velos.Info())
	tt.Equals(`{"addr":"","user":"","log":0}`, file.VelosSrv.Info())
}

